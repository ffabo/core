package de.bixilon.ffabo.core.main;

public class Strings {

	public final static int LOGIN_CRITICAL_ERROR = 1;

	public final static int COMMAND_ONLY_PLAYER = 2;
	public final static int COMMAND_NO_PERMISSION = 3;
	public final static int COMMAND_PLAYER_NOT_ONLINE = 11;
	public final static int COMMAND_PLAYER_NOT_FOUND = 12;

	public final static int COMMAND_GAMEMODE_USAGE = 4;
	public final static int COMMAND_GAMEMODE_SURVIVAL = 5;
	public final static int COMMAND_GAMEMODE_CREATIVE = 6;
	public final static int COMMAND_GAMEMODE_ADVENTURE = 7;
	public final static int COMMAND_GAMEMODE_SPECTATOR = 8;
	public final static int COMMAND_GAMEMODE_GAMEMODE_CHANGED_SELF = 9;
	public final static int COMMAND_GAMEMODE_GAMEMODE_CHANGED_OTHER = 10;

	public final static int COMMAND_MSG_USAGE = 13;
	public final static int COMMAND_MSG_SENT_SENDER = 14;
	public final static int COMMAND_MSG_SENT_RECEIVER = 15;
	public final static int COMMAND_MSG_FAIL_SELF = 16;

	public final static int COMMAND_KICK_USAGE = 17;
	public final static int COMMAND_KICK_SUCCESSFUL = 18;
	public final static int COMMAND_KICK_SUCCESSFUL_REASON = 19;
	public final static int COMMAND_KICK_BROADCAST = 20;
	public final static int COMMAND_KICK_BROADCAST_REASON = 21;
	public final static int COMMAND_KICK_KICKED = 22;
	public final static int COMMAND_KICK_KICKED_REASON = 23;
	public final static int COMMAND_KICK_CANT_KICK = 24;

	public final static int GENERAL_CONSOLE = 25;

	public final static int COMMAND_CLEAR_USAGE = 26;
	public final static int COMMAND_CLEAR_SELF = 27;
	public final static int COMMAND_CLEAR_OTHER = 28;
	public final static int COMMAND_CLEAR_OTHER_MESSAGE = 29;
	public final static int COMMAND_CLEAR_CAN_NOT = 30;

	public final static int COMMAND_FLY_USAGE = 31;
	public final static int COMMAND_FLY_CAN_NOT = 32;
	public final static int COMMAND_FLY_SELF_ENABLED = 33;
	public final static int COMMAND_FLY_SELF_DISABLED = 34;
	public final static int COMMAND_FLY_OTHER_ENABLED = 35;
	public final static int COMMAND_FLY_OTHER_DISABLED = 36;

	public final static int COMMAND_SPAWN_CANT_TELEPPORT = 39;
	public final static int COMMAND_SPAWN_TELEPORT_CANCELLED = 40;
	public final static int COMMAND_SPAWN_DONT_MOVE = 41;
	public final static int COMMAND_SPAWN_TELEPORTED = 42;
	public final static int COMMAND_SET_SPAWN = 43;

	public final static int EVENT_INFIGHT_INFIGHT = 37;
	public final static int EVENT_INFIGHT_OUTFIGHT = 38;

	public final static int DEATH_NO_KILL = 44;
	public final static int DEATH_KILLED_BOW = 45;
	public final static int DEATH_KILLED_BOW_MSG = 46;
	public final static int DEATH_KILLED_BURNED = 47;
	public final static int DEATH_KILLED_ITEM = 48;
	public final static int DEATH_KILLED_ITEM_MSG = 49;
	public final static int DEATH_KILLED_OTHER = 50;
	public final static int DEATH_BURNED = 51;
	public final static int DEATH_QUIT_REFOUND = 52;

	public final static int COMMAND_BUILD_ALLOWED = 53;
	public final static int EVENT_BUILD_BLACKLIST = 55;
	public final static int COMMAND_BUILD_DISABLED = 54;
	public final static int COMMAND_BUILD_REMOVEDALL = 56;

	public final static int COMMAND_TP_USAGE = 57;
	public final static int COMMAND_TP_SELF_PLAYER = 58;
	public final static int COMMAND_TP_TARGET_SELF_PLAYER = 64;
	public final static int COMMAND_TP_OTHER_PLAYER = 59;
	public final static int COMMAND_TP_TARGET_OTHER_PLAYER = 60;
	public final static int COMMAND_TP_START_OTHER_PLAYER = 65;
	public final static int COMMAND_TP_SELF_COORDINATE = 61;
	public final static int COMMAND_TP_OTHER_COORDINATE = 62;
	public final static int COMMAND_TP_TARGET_OTHER_COORDINATE = 63;
	public final static int COMMAND_TP_BAD_COORDINATES = 66;
	public final static int COMMAND_TP_CANT_TELEPORT_SELF = 67;

	public final static int COMMAND_HEAL_SELF = 68;
	public final static int COMMAND_HEAL_OTHER = 69;
	public final static int COMMAND_HEAL_TARGET_OTHER = 70;

	public final static int COMMAND_NV_SELF_ENABLED = 71;
	public final static int COMMAND_NV_SELF_DISABLED = 72;
	public final static int COMMAND_NV_OTHER_ENABLED = 73;
	public final static int COMMAND_NV_OTHER_DISABLED = 74;
	public final static int COMMAND_NV_USAGE = 75;

	public final static int SIGN_NO_PERMISSION = 76;
	public final static int SIGN_PLEASE_WAIT = 77;
	public final static int SIGN_NOT_ENOUGH_LEVEL = 78;

	public final static int VOTE_NOT_FOUND = 79;
	public final static int VOTE_NOT_ONLINE = 80;
	public final static int VOTE_UNKNOWN = 81;
	public final static int VOTE_BROADCAST = 82;
	public final static int COMMAND_UNKNOWN_COMMAND = 83;

	public static String getString(int name, String[] a) {
		switch (name) {
		case LOGIN_CRITICAL_ERROR:
			return "§cKritischer Fehler beim Anmelden! Bitte melde das umgehend!";
		case COMMAND_ONLY_PLAYER:
			return "§cNur ein Spieler kann diesen Command ausführen!";
		case COMMAND_NO_PERMISSION:
			return "§cDu hast (leider) keine Rechte, um diesen Befehl auszuführen!";
		case COMMAND_GAMEMODE_USAGE:
			return "§cBenutzung: §6/" + a[0] + " <0/1/2/3> [Spieler]";
		case COMMAND_GAMEMODE_SURVIVAL:
			return "Überleben";
		case COMMAND_GAMEMODE_CREATIVE:
			return "Kreativ";
		case COMMAND_GAMEMODE_ADVENTURE:
			return "Abenteuer";
		case COMMAND_GAMEMODE_SPECTATOR:
			return "Zuschauer!";
		case COMMAND_GAMEMODE_GAMEMODE_CHANGED_SELF:
			return "§3Du bist nun im §e" + a[0] + "-Modus§3!";
		case COMMAND_GAMEMODE_GAMEMODE_CHANGED_OTHER:
			return "§e" + a[0] + " §3ist nun im §e" + a[1] + "-Modus§3!";
		case COMMAND_PLAYER_NOT_ONLINE:
			return "§e" + a[0] + "§c ist nicht online!";
		case COMMAND_PLAYER_NOT_FOUND:
			return "§cUUID von §e" + a[0] + " §cnicht gefunden!";

		case COMMAND_MSG_USAGE:
			return "§cBenutzung: §6/msg <Spieler> <Nachricht>";
		case COMMAND_MSG_SENT_SENDER:
			return "§cDu §6-> §e" + a[0] + "§6: §7" + a[1];
		case COMMAND_MSG_SENT_RECEIVER:
			return "§e" + a[0] + " §6-> §cDir§6: §7" + a[1];
		case COMMAND_MSG_FAIL_SELF:
			return "§cDu kannst keine Selbstgespräche führen!";
		case COMMAND_KICK_USAGE:
			return "§cBenutzung §6/kick <Spieler> [Nachricht]";
		case COMMAND_KICK_SUCCESSFUL:
			return "§3Du hast §e" + a[0] + " §3erfolgreich gekickt!";
		case COMMAND_KICK_SUCCESSFUL_REASON:
			return "§3Du hast §e" + a[0] + " §3erfolgreich wegen §e" + a[1] + "§3gekickt!";
		case COMMAND_KICK_BROADCAST:
			return "§e" + a[0] + " §3wurde von §e" + a[1] + " §3gekickt!";
		case COMMAND_KICK_BROADCAST_REASON:
			return "§e" + a[0] + " §3wurde wegen §e" + a[1] + " von §e" + a[2] + " §3gekickt!";
		case COMMAND_KICK_KICKED:
			return "§cDu wurdest von §e" + a[0] + " §cgekickt!";
		case COMMAND_KICK_KICKED_REASON:
			return "§cDu wurdest von §e" + a[0] + " §cgekickt!\n\n§3Grund: §e" + a[1];
		case COMMAND_KICK_CANT_KICK:
			return "§cDu kannst " + a[0] + " §cnicht kicken!";

		case COMMAND_CLEAR_USAGE:
			return "§cBenutzung: §6/clear [Spieler]";
		case COMMAND_CLEAR_SELF:
			return "§3Dein Inventar wurde erfolgreich geleert!";
		case COMMAND_CLEAR_OTHER:
			return "§e" + a[0] + "§3s Inventar wurde erfolgreich geleert!";
		case COMMAND_CLEAR_OTHER_MESSAGE:
			return "§e" + a[0] + " §3hat deinen Inventar geleert!";
		case COMMAND_CLEAR_CAN_NOT:
			return "§cDu kannst das Inventar von §e" + a[0] + " §cnicht clearen!";

		case COMMAND_FLY_USAGE:
			return "§cBenutzung: §6/fly [Spieler]";
		case COMMAND_FLY_CAN_NOT:
			return "§cDu kannst §e" + a[0] + " §cnicht fliegen lassen!";
		case COMMAND_FLY_SELF_ENABLED:
			return "§3Du §akannst §3nun fliegen!";
		case COMMAND_FLY_SELF_DISABLED:
			return "§3Du kannst nun §cnicht meht §3fliegen!";
		case COMMAND_FLY_OTHER_ENABLED:
			return "§e" + a[0] + " §akann §3nun fliegen!";
		case COMMAND_FLY_OTHER_DISABLED:
			return "§e" + a[0] + " §3kann nun §cnicht meht §3fliegen!";
		case COMMAND_SPAWN_CANT_TELEPPORT:
			return "§cDu kannst dich nicht teleportieren, da du in einem Kampf bist!";
		case COMMAND_SPAWN_TELEPORT_CANCELLED:
			return "§4Du hast dich bewegt. Der Teleport wurde abgebrochen!";
		case COMMAND_SPAWN_DONT_MOVE:
			return "§7Du wirst in §e" + a[0] + " §7Sekunden teleportiert\n§cBeweg dich nicht!";
		case COMMAND_SPAWN_TELEPORTED:
			return "§6Du wurdest zum Spawn teleportiert!";
		case COMMAND_SET_SPAWN:
			return "§6Der Spawnpunkt wurde erfolgreich gesetzt(X: " + a[0] + "; Y: " + a[1] + ";Z: " + a[2] + ")!";

		case EVENT_INFIGHT_INFIGHT:
			return "§7Du bist nun für die nächsten §e" + a[0] + " §7Sekunden im Kampf.\n§cNicht verlassen!";
		case EVENT_INFIGHT_OUTFIGHT:
			return "§7Du bist nun §anicht mehr §7in einem Fight!";
		// killnachrichten
		case DEATH_NO_KILL:
			return "§e" + a[0] + " §7ist gestorben!";
		case DEATH_BURNED:
			return "§e" + a[0] + " §7verbrannte!";
		case DEATH_KILLED_BOW:
			return "§e" + a[0] + " §7wurde von §e" + a[1] + " §7erschossen!";
		case DEATH_KILLED_BOW_MSG:
			return "§e" + a[0] + " §7wurde von §e" + a[1] + " §7 mithilfe von §b[" + a[2] + "] §7erschossen!";
		case DEATH_KILLED_ITEM:
			return "§e" + a[0] + " §7wurde von §e" + a[1] + " §7abgestochen!";
		case DEATH_KILLED_ITEM_MSG:
			return "§e" + a[0] + " §7wurde von §e" + a[1] + " §7 mithilfe von §b[" + a[2] + "] §7abgestochen!";
		case DEATH_KILLED_BURNED:
			return "§e" + a[0] + " §7ist verbrannt während er im Kampf mit " + a[1] + " §7war.";
		case DEATH_KILLED_OTHER:
			return "§e" + a[0] + " §7wurde von §e" + a[1] + " §7getötet!";
		case DEATH_QUIT_REFOUND:
			return "§e" + a[1] + " §7wurde der Kill an §e" + a[0] + " §7entschädigt!";

		case COMMAND_BUILD_ALLOWED:
			return "§aDu kannst jetzt bauen!";
		case COMMAND_BUILD_DISABLED:
			return "§cDu kannst nun nicht mehr bauen!";
		case EVENT_BUILD_BLACKLIST:
			return "§cDieser Block steht auf der Blacklist!";
		case COMMAND_BUILD_REMOVEDALL:
			return "§aAlle Blöcke wurden entfernt!";

		case COMMAND_TP_USAGE: //
			return "§cBenutzung: §6/tp <Spieler/X Y Z> [Ziel/X Y Z]";
		case COMMAND_TP_SELF_PLAYER: //
			return "§7Du hast dich zu §e" + a[0] + " §7teleportiert.";
		case COMMAND_TP_TARGET_SELF_PLAYER: //
			return "§e" + a[0] + " §7hat sich zu dir teleportiert.";
		case COMMAND_TP_OTHER_PLAYER: //
			return "§e" + a[0] + " §7wurde zu §e" + a[1] + " §7teleportiert.";
		case COMMAND_TP_TARGET_OTHER_PLAYER: //
			return "§e" + a[0] + " §7wurde von §e" + a[1] + " §7zu §cdir §7teleportiert.";
		case COMMAND_TP_START_OTHER_PLAYER: //
			return "§cDu §7wurdest von §e" + a[1] + " §7zu " + a[0] + " teleportiert!";
		case COMMAND_TP_SELF_COORDINATE: //
			return "§7Du hast dich zu §c" + a[0] + " §7teleportiert.";
		case COMMAND_TP_OTHER_COORDINATE: //
			return "§cDu §7hast §e" + a[0] + " §7zu §e" + a[1] + " §7teleportiert.";
		case COMMAND_TP_TARGET_OTHER_COORDINATE:
			return "§cDu §7wurdest von §e" + a[0] + " §7zu §e" + a[1] + " §7teleportiert.";
		case COMMAND_TP_BAD_COORDINATES:
			return "§4Fehlerhafte Koordinaten!";
		case COMMAND_TP_CANT_TELEPORT_SELF:
			return "§cDu kannst dich nicht zu dir selbst teleportieren!";

		case COMMAND_HEAL_SELF:
			return "§6Du hast dich selbst geheilt!";
		case COMMAND_HEAL_OTHER:
			return "§6Du hast §e" + a[0] + " §6geheilt!";
		case COMMAND_HEAL_TARGET_OTHER:
			return "§cDu §6wurdest von §e" + a[0] + " §6geheilt!";
		case COMMAND_NV_SELF_ENABLED:
			return "§aDu hast nun Nachtsicht!";
		case COMMAND_NV_SELF_DISABLED:
			return "§cDu hast nun keine Nachtsicht mehr!";
		case COMMAND_NV_OTHER_ENABLED:
			return "§e" + a[0] + " §7hat nun Nachtsicht!";
		case COMMAND_NV_OTHER_DISABLED:
			return "§e" + a[0] + " §7hat nun §ckeine §7Nachtsicht mehr!";
		case COMMAND_NV_USAGE:
			return "§cBenutzung: §6/nv [Spieler]!";
		case SIGN_NO_PERMISSION:
			return "§cDas ist ein §6[VIP] §cSchild!";
		case SIGN_PLEASE_WAIT:
			return "§cBitte warte §e" + a[0] + " §cSekunden!";
		case SIGN_NOT_ENOUGH_LEVEL:
			return "§cDu brauchst mindestens §3" + a[0] + " §cLevel!";
			
		case VOTE_NOT_FOUND:
			return "§7Jemand, der noch §cnie §7 auf dem Server war, hat gevotet!\n§6Vote jetzt auch mit /vote!";
		case VOTE_NOT_ONLINE:
			return "§3" + a[0] + " §7hat gevotet, war aber nicht online!\n§7Er hat nur den Trostpreis bekommen!\n§6Vote jetzt auch mit /vote!";
		case VOTE_UNKNOWN:
			return "§7Ein unbekannter Spieler hat gevotet!\n$6Vote jetzt auch: /vote!";
		case VOTE_BROADCAST:
			return "§e" + a[0] + " §7hat gevotet!\n§6Vote jetzt auch: /vote";
		case COMMAND_UNKNOWN_COMMAND:
			return "§cDiesen Befehl gibt es leider nicht!";

		case GENERAL_CONSOLE:
			return "§4Konsole";
		}
		return "";
	}

	public static String getString(int name) {
		return getString(name, null);
	}

}

package de.bixilon.ffabo.core.main;

import java.util.HashMap;

import de.bixilon.ffabo.core.classes.Setting;

public class Settings {

	// static settings
	public static String prefix = "";

	public static String mysql_host = "172.18.0.1";
	public static int mysql_port = 3306;
	public static String mysql_db = "s24_fabo_skypvp";
	public static String mysql_user = "u24_qoVl2XWRkg";
	public static String mysql_pw = "XoeFQ7j7mmZCWUJrItDM";

	public static boolean debug = true; // Bypass login events, ....

	static HashMap<Integer, Setting> settings = new HashMap<Integer, Setting>();
	// static HashMap<String, Integer> settingsids = new HashMap<String, Integer>();
	// //key, id. key always lowercase!

	public static Setting getSetting(int key) {
		if (settings.containsKey(key))
			return settings.get(key);
		return null;
	}

	public static void addSetting(Setting setting) {
		settings.put(setting.getID(), setting);
	}

	public static void clearSettings() {
		settings = new HashMap<Integer, Setting>();
	}

	public static void updateSetting(Setting s) {
		settings.replace(s.getID(), s);
	}
}

package de.bixilon.ffabo.core.main;

import java.util.Timer;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import org.spigotmc.SpigotConfig;

import de.bixilon.ffabo.core.classes.BlockExplosionTimer;
import de.bixilon.ffabo.core.classes.BuildTimer;
import de.bixilon.ffabo.core.classes.InFight;
import de.bixilon.ffabo.core.classes.TeleportCooldown;
import de.bixilon.ffabo.core.commands.Build;
import de.bixilon.ffabo.core.commands.Clear;
import de.bixilon.ffabo.core.commands.Fly;
import de.bixilon.ffabo.core.commands.Gamemode;
import de.bixilon.ffabo.core.commands.Heal;
import de.bixilon.ffabo.core.commands.Kick;
import de.bixilon.ffabo.core.commands.MSG;
import de.bixilon.ffabo.core.commands.NightVision;
import de.bixilon.ffabo.core.commands.RemoveBlocks;
import de.bixilon.ffabo.core.commands.SetSpawn;
import de.bixilon.ffabo.core.commands.Spawn;
import de.bixilon.ffabo.core.commands.TP;
import de.bixilon.ffabo.core.commands.Test;
import de.bixilon.ffabo.core.events.AntiBreakEvent;
import de.bixilon.ffabo.core.events.AntiEnvironmentChange;
import de.bixilon.ffabo.core.events.AntiFallDamage;
import de.bixilon.ffabo.core.events.AutoLapis;
import de.bixilon.ffabo.core.events.BoostPlateEvent;
import de.bixilon.ffabo.core.events.BuildEvent;
import de.bixilon.ffabo.core.events.ChatEvent;
import de.bixilon.ffabo.core.events.ExplosionEvent;
import de.bixilon.ffabo.core.events.ItemXPEvent;
import de.bixilon.ffabo.core.events.InFightEvent;
import de.bixilon.ffabo.core.events.JoinEvent;
import de.bixilon.ffabo.core.events.JoinLeaveMessage;
import de.bixilon.ffabo.core.events.PingEvent;
import de.bixilon.ffabo.core.events.SignEvent;
import de.bixilon.ffabo.core.events.SpawnEvent;
import de.bixilon.ffabo.core.events.TeleportEvent;
import de.bixilon.ffabo.core.events.VoteEvent;
import de.bixilon.ffabo.core.mysql.Driver;
import de.bixilon.ffabo.core.mysql.MySQL;

public class Main extends JavaPlugin {
	public static MySQL mysql;
	public static Main instance;

	public void onEnable() {
		getServer().getConsoleSender().sendMessage("§5Starting SkyPvP...");
		instance = this;

		getServer().getConsoleSender().sendMessage("§7Connecting to MySQL...");

		mysql = new MySQL(new Driver(Settings.mysql_host, Settings.mysql_port, Settings.mysql_db, Settings.mysql_user,
				Settings.mysql_pw));
		if (mysql.isConnected())
			getServer().getConsoleSender().sendMessage("§aMySQL connected!");
		else {
			getServer().getConsoleSender().sendMessage("§4Connection to MySQL failed!");
			Bukkit.getShutdownMessage();
		}
		getServer().getConsoleSender().sendMessage("§7Fetching data from MySQL...");
		mysql.fetch();
		getServer().getConsoleSender().sendMessage("§aData fetched!");

		registerEvents();
		registerCommands();
		registerTimers();

		initSpigotConfig();
		register();
		getServer().getConsoleSender().sendMessage("§aSkyPvP Core started!");
	}

	public void onDisable() {
		BuildEvent.destroyAll();

	}

	public void registerEvents() {
		getServer().getPluginManager().registerEvents(new JoinLeaveMessage(), this);
		getServer().getPluginManager().registerEvents(new JoinEvent(), this);
		getServer().getPluginManager().registerEvents(new ChatEvent(), this);
		getServer().getPluginManager().registerEvents(new InFightEvent(), this);
		getServer().getPluginManager().registerEvents(new TeleportEvent(), this);
		getServer().getPluginManager().registerEvents(new ItemXPEvent(), this);
		getServer().getPluginManager().registerEvents(new BuildEvent(), this);
		getServer().getPluginManager().registerEvents(new AntiBreakEvent(), this);
		getServer().getPluginManager().registerEvents(new ExplosionEvent(), this);
		getServer().getPluginManager().registerEvents(new SignEvent(), this);
		getServer().getPluginManager().registerEvents(new AutoLapis(), this);
		getServer().getPluginManager().registerEvents(new AntiFallDamage(), this);
		getServer().getPluginManager().registerEvents(new AntiEnvironmentChange(), this);
		getServer().getPluginManager().registerEvents(new BoostPlateEvent(), this);
		getServer().getPluginManager().registerEvents(new SpawnEvent(), this);
		getServer().getPluginManager().registerEvents(new VoteEvent(), this);
		getServer().getPluginManager().registerEvents(new PingEvent(), this);
	}

	public void registerCommands() {
		this.getCommand("gamemode").setExecutor(new Gamemode());
		this.getCommand("msg").setExecutor(new MSG());
		this.getCommand("kick").setExecutor(new Kick());
		this.getCommand("clear").setExecutor(new Clear());
		this.getCommand("fly").setExecutor(new Fly());
		this.getCommand("spawn").setExecutor(new Spawn());
		this.getCommand("setspawn").setExecutor(new SetSpawn());
		this.getCommand("build").setExecutor(new Build());
		this.getCommand("destroyblocks").setExecutor(new RemoveBlocks());
		this.getCommand("tp").setExecutor(new TP());
		this.getCommand("heal").setExecutor(new Heal());
		this.getCommand("nightvision").setExecutor(new NightVision());
		this.getCommand("test").setExecutor(new Test());

	}

	public static void register() {
		BuildEvent.init();
		BlockExplosionTimer.init();
		AutoLapis.init();
		AntiBreakEvent.init();
		SpawnEvent.init();
	}

	public static Main getInstance() {
		return instance;
	}

	public static void initSpigotConfig() {
		Bukkit.getWorld("world").setGameRuleValue("doFireTick", "false");
		SpigotConfig.bungee = false;
		SpigotConfig.unknownCommandMessage = Strings.getString(Strings.COMMAND_UNKNOWN_COMMAND);
		SpigotConfig.restartOnCrash = true;

	}

	@SuppressWarnings("deprecation")
	public void registerTimers() {

		Timer infighttimer = new Timer();
		infighttimer.schedule(new InFight(), 0, 1000);

		Timer teleporttimer = new Timer();
		teleporttimer.schedule(new TeleportCooldown(), 0, 1000);
		getServer().getScheduler().scheduleSyncRepeatingTask(this, new BuildTimer(), 0L, 1L);
		getServer().getScheduler().scheduleSyncRepeatingTask(this, new BlockExplosionTimer(), 0L, 5L);

	}
}

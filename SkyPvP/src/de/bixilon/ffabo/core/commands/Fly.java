package de.bixilon.ffabo.core.commands;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.bixilon.ffabo.core.classes.User;
import de.bixilon.ffabo.core.main.Strings;

public class Fly implements CommandExecutor {

	static List<UUID> flying = new ArrayList<UUID>();

	public boolean onCommand(CommandSender cs, Command cmd, String alias, String[] args) {
		boolean allowed = false;
		if (!(cs instanceof Player)) {
			// Console
			allowed = true;
		} else {
			Player p = (Player) cs;
			User u = User.getUser(p.getUniqueId());
			if (args.length == 0) {
				if (u.hasPermission(1))
					allowed = true;
			} else if (args.length == 1) {
				if (u.hasPermission(2))
					allowed = true;
			}
		}
		if (!allowed) {
			cs.sendMessage(Strings.getString(Strings.COMMAND_NO_PERMISSION));
			return false;
		}
		if (args.length == 0) {
			if (!(cs instanceof Player)) {
				cs.sendMessage(Strings.getString(Strings.COMMAND_ONLY_PLAYER));
				return false;
			}
			Player p = (Player) cs;

			if (flying.contains(p.getUniqueId())) {
				flying.remove(p.getUniqueId());
				p.setAllowFlight(false);
				p.setFlying(false);
				cs.sendMessage(Strings.getString(Strings.COMMAND_FLY_SELF_DISABLED));
			} else {
				flying.add(p.getUniqueId());
				p.setAllowFlight(true);
				p.setFlying(true);
				cs.sendMessage(Strings.getString(Strings.COMMAND_FLY_SELF_ENABLED));
			}
		} else if (args.length == 1) {
			User target = User.getUser(args[0]);
			if (target == null) {
				cs.sendMessage(Strings.getString(Strings.COMMAND_PLAYER_NOT_FOUND, new String[] { args[0] }));
			} else if (target.getPlayer() == null) {
				cs.sendMessage(
						Strings.getString(Strings.COMMAND_PLAYER_NOT_ONLINE, new String[] { target.formatUserName() }));
			} else {

				if (flying.contains(target.getUUID())) {
					flying.remove(target.getUUID());
					target.getPlayer().setAllowFlight(false);
					target.getPlayer().setFlying(false);
					cs.sendMessage(Strings.getString(Strings.COMMAND_FLY_OTHER_DISABLED,
							new String[] { target.formatUserName() }));
					target.getPlayer().sendMessage(Strings.getString(Strings.COMMAND_FLY_SELF_DISABLED));
				} else {
					flying.add(target.getUUID());
					target.getPlayer().setAllowFlight(true);
					target.getPlayer().setFlying(true);
					cs.sendMessage(Strings.getString(Strings.COMMAND_FLY_OTHER_ENABLED,
							new String[] { target.formatUserName() }));
					target.getPlayer().sendMessage(Strings.getString(Strings.COMMAND_FLY_SELF_ENABLED));
				}
			}
		} else
			cs.sendMessage(Strings.getString(Strings.COMMAND_FLY_USAGE));

		return true;

	}
}
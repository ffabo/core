package de.bixilon.ffabo.core.commands;

import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.bixilon.ffabo.core.classes.User;
import de.bixilon.ffabo.core.main.Strings;

public class TP implements CommandExecutor {

	public boolean onCommand(CommandSender cs, Command cmd, String alias, String[] args) {
		if (cs instanceof Player) {
			// Console
			Player p = (Player) cs;
			User u = User.getUser(p.getUniqueId());
			if (!u.hasPermission(15)) {
				u.getPlayer().sendMessage(Strings.getString(Strings.COMMAND_NO_PERMISSION));
				return false;
			}
		}
		if (args.length == 1) {
			// tp <player>
			if (!(cs instanceof Player)) {
				cs.sendMessage(Strings.getString(Strings.COMMAND_ONLY_PLAYER));
				return false;
			}
			Player p = (Player) cs;
			User u = User.getUser(p.getUniqueId());
			User target = User.getUser(args[0]);
			if (target == null) {
				u.getPlayer()
						.sendMessage(Strings.getString(Strings.COMMAND_PLAYER_NOT_FOUND, new String[] { args[0] }));
				return false;
			}
			if (target.getPlayer() == null) {
				u.getPlayer().sendMessage(
						Strings.getString(Strings.COMMAND_PLAYER_NOT_ONLINE, new String[] { target.formatUserName() }));
				return false;
			}
			if (target.equals(u)) {
				// self
				u.getPlayer().sendMessage(Strings.getString(Strings.COMMAND_TP_CANT_TELEPORT_SELF));
				return false;
			}
			if (!u.hasPermission(16))
				target.getPlayer().sendMessage(
						Strings.getString(Strings.COMMAND_TP_TARGET_SELF_PLAYER, new String[] { u.formatUserName() }));
			cs.sendMessage(Strings.getString(Strings.COMMAND_TP_SELF_PLAYER, new String[] { target.formatUserName() }));
			u.getPlayer().teleport(target.getPlayer().getLocation());
		} else if (args.length == 2) {
			// tp <player> <targetplayer>
			Player p = (Player) cs;
			User u = User.getUser(p.getUniqueId());
			User player = User.getUser(args[0]);
			User target = User.getUser(args[1]);
			if (player == null) {
				u.getPlayer()
						.sendMessage(Strings.getString(Strings.COMMAND_PLAYER_NOT_FOUND, new String[] { args[0] }));
				return false;
			}
			if (player.getPlayer() == null) {
				u.getPlayer().sendMessage(
						Strings.getString(Strings.COMMAND_PLAYER_NOT_ONLINE, new String[] { player.formatUserName() }));
				return false;
			}
			if (target == null) {
				u.getPlayer()
						.sendMessage(Strings.getString(Strings.COMMAND_PLAYER_NOT_FOUND, new String[] { args[1] }));
				return false;
			}
			if (target.getPlayer() == null) {
				u.getPlayer().sendMessage(
						Strings.getString(Strings.COMMAND_PLAYER_NOT_ONLINE, new String[] { target.formatUserName() }));
				return false;
			}
			if (target.equals(player)) {
				// self
				u.getPlayer().sendMessage(Strings.getString(Strings.COMMAND_TP_CANT_TELEPORT_SELF));
				return false;
			}
			player.getPlayer().teleport(target.getPlayer().getLocation());
			player.getPlayer().sendMessage(Strings.getString(Strings.COMMAND_TP_START_OTHER_PLAYER,
					new String[] { target.formatUserName(), u.formatUserName() }));
			u.getPlayer().sendMessage(Strings.getString(Strings.COMMAND_TP_OTHER_PLAYER,
					new String[] { player.formatUserName(), target.formatUserName() }));
			target.getPlayer().sendMessage(Strings.getString(Strings.COMMAND_TP_TARGET_OTHER_PLAYER,
					new String[] { player.formatUserName(), u.formatUserName() }));

		} else if (args.length == 3) {
			// tp <X> <Y> <Z>
			if (!(cs instanceof Player)) {
				cs.sendMessage(Strings.getString(Strings.COMMAND_ONLY_PLAYER));
				return false;
			}
			Player p = (Player) cs;
			User u = User.getUser(p.getUniqueId());
			Location loc = null;
			try {
				loc = new Location(u.getPlayer().getWorld(), Double.parseDouble(args[0]), Double.parseDouble(args[1]),
						Double.parseDouble(args[2]));
			} catch (Exception e) {
				u.getPlayer().sendMessage(Strings.getString(Strings.COMMAND_TP_BAD_COORDINATES));
				return false;
			}
			u.getPlayer().teleport(loc);
			u.getPlayer().sendMessage(Strings.getString(Strings.COMMAND_TP_SELF_COORDINATE,
					new String[] { loc.getX() + " " + loc.getY() + " " + loc.getZ() }));

		} else if (args.length == 4) {
			// tp <player> <X> <Y> <Z>
			Player p = (Player) cs;
			User u = User.getUser(p.getUniqueId());
			User target = User.getUser(args[0]);
			if (target == null) {
				u.getPlayer()
						.sendMessage(Strings.getString(Strings.COMMAND_PLAYER_NOT_FOUND, new String[] { args[1] }));
				return false;
			}
			if (target.getPlayer() == null) {
				u.getPlayer().sendMessage(
						Strings.getString(Strings.COMMAND_PLAYER_NOT_ONLINE, new String[] { target.formatUserName() }));
				return false;
			}
			Location loc = null;
			try {
				loc = new Location(u.getPlayer().getWorld(), Double.parseDouble(args[1]), Double.parseDouble(args[2]),
						Double.parseDouble(args[3]));
			} catch (Exception e) {
				u.getPlayer().sendMessage(Strings.getString(Strings.COMMAND_TP_BAD_COORDINATES));
				return false;
			}
			target.getPlayer().teleport(loc);
			if (!target.equals(u)) {
				// self
				target.getPlayer().sendMessage(Strings.getString(Strings.COMMAND_TP_TARGET_OTHER_COORDINATE,
						new String[] { u.formatUserName(), loc.getX() + " " + loc.getY() + " " + loc.getZ() }));
				u.getPlayer().sendMessage(Strings.getString(Strings.COMMAND_TP_OTHER_COORDINATE,
						new String[] { target.formatUserName(), loc.getX() + " " + loc.getY() + " " + loc.getZ() }));
				return false;
			}

			u.getPlayer().sendMessage(Strings.getString(Strings.COMMAND_TP_SELF_COORDINATE,
					new String[] { loc.getX() + " " + loc.getY() + " " + loc.getZ() }));

		} else
			cs.sendMessage(Strings.getString(Strings.COMMAND_TP_USAGE));
		return true;

	}
}
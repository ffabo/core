package de.bixilon.ffabo.core.commands;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import de.bixilon.ffabo.core.classes.Serializer;
import de.bixilon.ffabo.core.main.Main;

public class Test implements CommandExecutor {

	static List<UUID> flying = new ArrayList<UUID>();

	@SuppressWarnings("deprecation")
	public boolean onCommand(CommandSender cs, Command cmd, String alias, String[] args) {
		Player p = (Player) cs;
		cs.sendMessage(Serializer.itemStackArrayToBase64(new ItemStack[] {p.getItemInHand()}));
		Main.getInstance().getLogger().info(p.getItemInHand().getTypeId() + ": " + Serializer.itemStackArrayToBase64(new ItemStack[] {p.getItemInHand()}));
		return true;

	}
}
package de.bixilon.ffabo.core.commands;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.bixilon.ffabo.core.classes.User;
import de.bixilon.ffabo.core.events.BuildEvent;
import de.bixilon.ffabo.core.main.Strings;

public class RemoveBlocks implements CommandExecutor {
	public static List<UUID> building = new ArrayList<UUID>();

	public boolean onCommand(CommandSender cs, Command cmd, String alias, String[] args) {
		if (cs instanceof Player) {
			User u = User.getUser(((Player) cs).getUniqueId());
			if (!u.hasPermission(14)) {
				u.getPlayer().sendMessage(Strings.getString(Strings.COMMAND_NO_PERMISSION));
				return true;
			}
		}

		BuildEvent.destroyAll();
		cs.sendMessage(Strings.getString(Strings.COMMAND_BUILD_REMOVEDALL));
		return true;

	}
}
package de.bixilon.ffabo.core.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.bixilon.ffabo.core.classes.User;
import de.bixilon.ffabo.core.main.Strings;

public class MSG implements CommandExecutor {

	public boolean onCommand(CommandSender cs, Command cmd, String alias, String[] args) {
		if (args.length > 1) {
			User targetu = User.getUser(args[0]);
			if (targetu == null)
				cs.sendMessage(Strings.getString(Strings.COMMAND_PLAYER_NOT_FOUND, new String[] { args[0] }));
			else if (targetu.getPlayer() != null) {
				if (targetu.getPlayer().getName() == cs.getName()) {
					cs.sendMessage(Strings.getString(Strings.COMMAND_MSG_FAIL_SELF));
					return false;
				}
				String message = "";
				for (int i = 1; i < args.length; i++) {
					message += args[i] + " ";
				}
				cs.sendMessage(Strings.getString(Strings.COMMAND_MSG_SENT_SENDER,
						new String[] { targetu.formatUserName(), message }));
				String sender;
				if (cs instanceof Player) {
					Player p = (Player) cs;
					User u = User.getUser(p.getUniqueId());
					sender = u.formatUserName();
				} else
					sender = Strings.getString(Strings.GENERAL_CONSOLE);
				targetu.getPlayer().sendMessage(
						Strings.getString(Strings.COMMAND_MSG_SENT_RECEIVER, new String[] { sender, message }));
			} else
				cs.sendMessage(Strings.getString(Strings.COMMAND_PLAYER_NOT_ONLINE,
						new String[] { targetu.formatUserName() }));

		} else
			cs.sendMessage(Strings.getString(Strings.COMMAND_MSG_USAGE));

		return true;
	}
}
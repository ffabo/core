package de.bixilon.ffabo.core.commands;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.bixilon.ffabo.core.classes.User;
import de.bixilon.ffabo.core.main.Strings;

public class Heal implements CommandExecutor {

	static List<UUID> flying = new ArrayList<UUID>();

	public boolean onCommand(CommandSender cs, Command cmd, String alias, String[] args) {
		boolean allowed = false;
		if (cs instanceof Player) {
			// Console
			Player p = (Player) cs;
			User u = User.getUser(p.getUniqueId());
			if (args.length == 0) {
				if (u.hasPermission(17))
					allowed = true;
			} else if (args.length == 1) {
				if (u.hasPermission(18))
					allowed = true;
			}
		}
		if (!allowed) {
			cs.sendMessage(Strings.getString(Strings.COMMAND_NO_PERMISSION));
			return false;
		}
		if (args.length == 0) {
			if (!(cs instanceof Player)) {
				cs.sendMessage(Strings.getString(Strings.COMMAND_ONLY_PLAYER));
				return false;
			}
			Player p = (Player) cs;
			p.setHealth(20);
			p.setFoodLevel(20);
			p.sendMessage(Strings.getString(Strings.COMMAND_HEAL_SELF));
		} else if (args.length == 1) {
			User target = User.getUser(args[0]);
			if (target == null) {
				cs.sendMessage(Strings.getString(Strings.COMMAND_PLAYER_NOT_FOUND, new String[] { args[0] }));
			} else if (target.getPlayer() == null) {
				cs.sendMessage(
						Strings.getString(Strings.COMMAND_PLAYER_NOT_ONLINE, new String[] { target.formatUserName() }));
			} else {
				String sender = Strings.getString(Strings.GENERAL_CONSOLE);
				if (cs instanceof Player)
					sender = User.getUser(((Player) cs).getUniqueId()).formatUserName();
				target.getPlayer().setHealth(20);
				target.getPlayer().setFoodLevel(20);
				target.getPlayer()
						.sendMessage(Strings.getString(Strings.COMMAND_HEAL_TARGET_OTHER, new String[] { sender }));

				cs.sendMessage(Strings.getString(Strings.COMMAND_HEAL_OTHER, new String[] { target.formatUserName() }));
			}
		} else
			cs.sendMessage(Strings.getString(Strings.COMMAND_FLY_USAGE));

		return true;

	}
}
package de.bixilon.ffabo.core.commands;

import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.bixilon.ffabo.core.classes.Setting;
import de.bixilon.ffabo.core.classes.User;
import de.bixilon.ffabo.core.main.Main;
import de.bixilon.ffabo.core.main.Settings;
import de.bixilon.ffabo.core.main.Strings;

public class SetSpawn implements CommandExecutor {

	public boolean onCommand(CommandSender cs, Command cmd, String alias, String[] args) {
		if (!(cs instanceof Player)) {
			cs.sendMessage(Strings.getString(Strings.COMMAND_ONLY_PLAYER));
			return false;
		}
		User u = User.getUser(((Player) cs).getUniqueId());
		if (!u.hasPermission(10)) {
			u.getPlayer().sendMessage(Strings.getString(Strings.COMMAND_NO_PERMISSION));
			;
			return false;
		}
		Location l = u.getPlayer().getLocation();
		String loc = l.getWorld().getName() + ";" + l.getX() + ";" + l.getY() + ";" + l.getZ() + ";" + l.getYaw() + ";"
				+ l.getPitch();
		u.getPlayer().sendMessage(Strings.getString(Strings.COMMAND_SET_SPAWN,
				new String[] { String.valueOf(l.getX()), String.valueOf(l.getY()), String.valueOf(l.getZ()) }));
		Main.mysql.setSpawnPoint(loc);
		Settings.updateSetting(
				new Setting(5, Settings.getSetting(5).getKey(), loc, Settings.getSetting(5).getComment()));

		return true;

	}
}
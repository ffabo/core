package de.bixilon.ffabo.core.commands;

import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.bixilon.ffabo.core.classes.User;
import de.bixilon.ffabo.core.main.Strings;

public class Gamemode implements CommandExecutor {

	public boolean onCommand(CommandSender cs, Command cmd, String alias, String[] args) {
		if (!(cs instanceof Player)) {
			// Console
			cs.sendMessage(Strings.getString(Strings.COMMAND_ONLY_PLAYER));
			return false;
		}
		Player p = (Player) cs;
		User u = User.getUser(p.getUniqueId());

		if (u.hasPermission(3)) {
			if (args.length == 1 || args.length == 2) {
				GameMode gm = numberToGameMode(args[0]);
				if (gm != null) {
					String gamemode = numberToGameModeString(args[0]);
					if (args.length == 1) {
						p.setGameMode(gm);
						cs.sendMessage(Strings.getString(Strings.COMMAND_GAMEMODE_GAMEMODE_CHANGED_SELF,
								new String[] { gamemode }));
					} else if (u.hasPermission(4)) {
						User target = User.getUser(args[1]);
						if (target == null)
							cs.sendMessage(
									Strings.getString(Strings.COMMAND_PLAYER_NOT_FOUND, new String[] { args[1] }));
						else if (target.getPlayer() == null) {
							cs.sendMessage(Strings.getString(Strings.COMMAND_PLAYER_NOT_ONLINE,
									new String[] { target.formatUserName() }));

						} else {
							if(target.getPlayer() != p) //set own gamemode
							cs.sendMessage(Strings.getString(Strings.COMMAND_GAMEMODE_GAMEMODE_CHANGED_OTHER,
									new String[] { target.formatUserName(), gamemode }));
							target.getPlayer().sendMessage(Strings.getString(
									Strings.COMMAND_GAMEMODE_GAMEMODE_CHANGED_SELF, new String[] { gamemode }));
							target.getPlayer().setGameMode(gm);
						}

					} else
						cs.sendMessage(Strings.getString(Strings.COMMAND_NO_PERMISSION));
				} else
					cs.sendMessage(Strings.getString(Strings.COMMAND_GAMEMODE_USAGE, new String[] {cmd.getLabel()}));
			} else {
				cs.sendMessage(Strings.getString(Strings.COMMAND_GAMEMODE_USAGE, new String[] {cmd.getLabel()}));
			}

		} else {
			cs.sendMessage(Strings.getString(Strings.COMMAND_NO_PERMISSION));
		}

		return true;
	}

	public GameMode numberToGameMode(String gm) {
		switch (gm) {
		case "0":
			return GameMode.SURVIVAL;
		case "1":
			return GameMode.CREATIVE;
		case "2":
			return GameMode.ADVENTURE;
		case "3":
			return GameMode.SPECTATOR;
		}
		return null;
	}

	public String numberToGameModeString(String gm) {
		switch (gm) {
		case "0":
			return Strings.getString(Strings.COMMAND_GAMEMODE_SURVIVAL);
		case "1":
			return Strings.getString(Strings.COMMAND_GAMEMODE_CREATIVE);
		case "2":
			return Strings.getString(Strings.COMMAND_GAMEMODE_ADVENTURE);
		case "3":
			return Strings.getString(Strings.COMMAND_GAMEMODE_SPECTATOR);
		}
		return null;
	}

}
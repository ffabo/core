package de.bixilon.ffabo.core.commands;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import de.bixilon.ffabo.core.classes.User;
import de.bixilon.ffabo.core.main.Strings;

public class NightVision implements CommandExecutor {
	
	static List<UUID> nving = new ArrayList<UUID>();

	public boolean onCommand(CommandSender cs, Command cmd, String alias, String[] args) {
		boolean allowed = false;
		if (!(cs instanceof Player)) {
			// Console
			allowed = true;
		} else {
			Player p = (Player) cs;
			User u = User.getUser(p.getUniqueId());
			if (args.length == 0) {
				if (u.hasPermission(19))
					allowed = true;
			} else if (args.length == 1) {
				if (u.hasPermission(20))
					allowed = true;
			}
		}
		if (!allowed) {
			cs.sendMessage(Strings.getString(Strings.COMMAND_NO_PERMISSION));
			return false;
		}
		if (args.length == 0) {
			if (!(cs instanceof Player)) {
				cs.sendMessage(Strings.getString(Strings.COMMAND_ONLY_PLAYER));
				return false;
			}
			Player p = (Player) cs;
			if(nving.contains(p.getUniqueId())) {
				nving.remove(p.getUniqueId());
				p.sendMessage(Strings.getString(Strings.COMMAND_NV_SELF_DISABLED));
				p.removePotionEffect(PotionEffectType.NIGHT_VISION);
			}else {
				nving.add(p.getUniqueId());
				p.sendMessage(Strings.getString(Strings.COMMAND_NV_SELF_ENABLED));
				p.addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION, Integer.MAX_VALUE, 1));
			}
		} else if (args.length == 1) {
			User target = User.getUser(args[0]);
			if (target == null) {
				cs.sendMessage(Strings.getString(Strings.COMMAND_PLAYER_NOT_FOUND, new String[] { args[0] }));
			} else if (target.getPlayer() == null) {
				cs.sendMessage(
						Strings.getString(Strings.COMMAND_PLAYER_NOT_ONLINE, new String[] { target.formatUserName() }));
			} else {
				
				
				
				if(nving.contains(target.getPlayer().getUniqueId())) {
					nving.remove(target.getPlayer().getUniqueId());
					target.getPlayer().sendMessage(Strings.getString(Strings.COMMAND_NV_SELF_DISABLED));
					cs.sendMessage(Strings.getString(Strings.COMMAND_NV_OTHER_DISABLED, new String[] {target.formatUserName()}));
					target.getPlayer().removePotionEffect(PotionEffectType.NIGHT_VISION);
				}else {
					nving.add(target.getPlayer().getUniqueId());
					target.getPlayer().sendMessage(Strings.getString(Strings.COMMAND_NV_SELF_ENABLED));
					cs.sendMessage(Strings.getString(Strings.COMMAND_NV_OTHER_ENABLED, new String[] {target.formatUserName()}));
					target.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION, Integer.MAX_VALUE, 1));
				}
			}
		} else
			cs.sendMessage(Strings.getString(Strings.COMMAND_NV_USAGE));

		return true;

	}
}

package de.bixilon.ffabo.core.commands;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.bixilon.ffabo.core.classes.TeleportCooldown;
import de.bixilon.ffabo.core.classes.User;
import de.bixilon.ffabo.core.main.Settings;
import de.bixilon.ffabo.core.main.Strings;

public class Spawn implements CommandExecutor {

	public boolean onCommand(CommandSender cs, Command cmd, String alias, String[] args) {
		if (!(cs instanceof Player)) {
			cs.sendMessage(Strings.getString(Strings.COMMAND_ONLY_PLAYER));
			return false;
		}
		User u = User.getUser(((Player) cs).getUniqueId());
		if (u.isInFight()) {
			cs.sendMessage(Strings.getString(Strings.COMMAND_SPAWN_CANT_TELEPPORT));
			return false;
		}
		if (u.hasPermission(9)) {
			teleportToSpawn(u.getPlayer());
			return true;
		}
		TeleportCooldown.spawn_teleport.put(u.getUUID(), (int) (System.currentTimeMillis() / 1000));
		u.getPlayer().sendMessage(Strings.getString(Strings.COMMAND_SPAWN_DONT_MOVE,
				new String[] { (String) Settings.getSetting(3).getValue() }));

		return true;

	}

	public static void teleportToSpawn(Player p) {
		p.teleport(getSpawn());
		p.sendMessage(Strings.getString(Strings.COMMAND_SPAWN_TELEPORTED));

	}

	public static Location getSpawn() {
		String[] loc_raw = Settings.getSetting(5).getValue().toString().split(";");
		Location spawnpoint = new Location(Bukkit.getWorld(loc_raw[0]), Double.parseDouble(loc_raw[1]),
				Double.parseDouble(loc_raw[2]), Double.parseDouble(loc_raw[3]), Float.parseFloat(loc_raw[4]),
				Float.parseFloat(loc_raw[5]));

		return spawnpoint;
	}
}
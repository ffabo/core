package de.bixilon.ffabo.core.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.bixilon.ffabo.core.classes.User;
import de.bixilon.ffabo.core.main.Strings;

public class Kick implements CommandExecutor {

	public boolean onCommand(CommandSender cs, Command cmd, String alias, String[] args) {
		boolean allowed = false;
		if (!(cs instanceof Player)) {
			// Console
			allowed = true;
		} else {
			Player p = (Player) cs;
			User u = User.getUser(p.getUniqueId());
			if (u.hasPermission(5))
				allowed = true;
		}
		if (!allowed) {
			cs.sendMessage(Strings.getString(Strings.COMMAND_NO_PERMISSION));
			return false;
		}
		if (args.length > 0) {
			User targetu = User.getUser(args[0]);

			if (targetu == null) {
				cs.sendMessage(Strings.getString(Strings.COMMAND_PLAYER_NOT_FOUND, new String[] { args[0] }));
			} else if (targetu.getPlayer() != null) {
				boolean canKick = false;
				String kicker = "";
				if (cs instanceof Player) {
					if (User.getUser(((Player) cs).getUniqueId()).getGroup().getPriority() <= targetu.getGroup()
							.getPriority()) {
						canKick = true;
						kicker = User.getUser(((Player) cs).getUniqueId()).formatUserName();
					}

				} else {
					kicker = Strings.getString(Strings.GENERAL_CONSOLE);
					canKick = true;
				}
				if (!canKick) {
					cs.sendMessage(Strings.getString(Strings.COMMAND_KICK_CANT_KICK,
							new String[] { targetu.formatUserName() }));
					return false;
				}
				if (args.length == 1) { // "no" reason
					targetu.getPlayer()
							.kickPlayer(Strings.getString(Strings.COMMAND_KICK_KICKED, new String[] { kicker }));

					cs.sendMessage(Strings.getString(Strings.COMMAND_KICK_SUCCESSFUL,
							new String[] { targetu.formatUserName() }));
					for (Player p : Bukkit.getServer().getOnlinePlayers()) {
						if (p.getName() != cs.getName())
							p.sendMessage(Strings.getString(Strings.COMMAND_KICK_BROADCAST,
									new String[] { targetu.formatUserName(), kicker }));
					}
				} else { // readon given
					String reason = "";
					for (int i = 1; i < args.length; i++) {
						reason += args[i] + " ";
					}
					targetu.getPlayer().kickPlayer(
							Strings.getString(Strings.COMMAND_KICK_KICKED_REASON, new String[] { kicker, reason }));

					cs.sendMessage(Strings.getString(Strings.COMMAND_KICK_SUCCESSFUL,
							new String[] { targetu.formatUserName(), reason }));
					for (Player p : Bukkit.getServer().getOnlinePlayers()) {
						if (p.getName() != cs.getName())
							p.sendMessage(Strings.getString(Strings.COMMAND_KICK_BROADCAST,
									new String[] { targetu.formatUserName(), reason, kicker }));
					}
				}

			} else
				cs.sendMessage(Strings.getString(Strings.COMMAND_PLAYER_NOT_ONLINE,
						new String[] { targetu.formatUserName() }));
		}else
			cs.sendMessage(Strings.getString(Strings.COMMAND_KICK_USAGE));

		return true;

	}
}
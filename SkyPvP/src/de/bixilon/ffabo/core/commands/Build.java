package de.bixilon.ffabo.core.commands;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.bixilon.ffabo.core.classes.User;
import de.bixilon.ffabo.core.main.Strings;

public class Build implements CommandExecutor {
	public static List<UUID> building = new ArrayList<UUID>();

	public boolean onCommand(CommandSender cs, Command cmd, String alias, String[] args) {
		if (!(cs instanceof Player)) {
			cs.sendMessage(Strings.getString(Strings.COMMAND_ONLY_PLAYER));
			return false;
		}
		User u = User.getUser(((Player) cs).getUniqueId());
		if (!u.hasPermission(13)) {
			u.getPlayer().sendMessage(Strings.getString(Strings.COMMAND_NO_PERMISSION));
			return true;
		}

		if (building.contains(u.getPlayer().getUniqueId())) {
			building.remove(u.getPlayer().getUniqueId());
			cs.sendMessage(Strings.getString(Strings.COMMAND_BUILD_DISABLED));
		} else {
			building.add(u.getPlayer().getUniqueId());
			cs.sendMessage(Strings.getString(Strings.COMMAND_BUILD_ALLOWED));
		}
		return true;

	}
}
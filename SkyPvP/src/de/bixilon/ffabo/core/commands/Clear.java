package de.bixilon.ffabo.core.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.bixilon.ffabo.core.classes.User;
import de.bixilon.ffabo.core.main.Strings;

public class Clear implements CommandExecutor {

	public boolean onCommand(CommandSender cs, Command cmd, String alias, String[] args) {
		boolean allowed = false;
		if (!(cs instanceof Player)) {
			// Console
			allowed = true;
		} else {
			Player p = (Player) cs;
			User u = User.getUser(p.getUniqueId());
			if (args.length == 0) {
				if (u.hasPermission(6))
					allowed = true;
			} else if (args.length == 1) {
				if (u.hasPermission(7))
					allowed = true;
			}
		}
		if (!allowed) {
			cs.sendMessage(Strings.getString(Strings.COMMAND_NO_PERMISSION));
			return false;
		}
		if (args.length == 0) {
			if (!(cs instanceof Player)) {
				cs.sendMessage(Strings.getString(Strings.COMMAND_ONLY_PLAYER));
				return false;
			}
			Player p = (Player) cs;
			p.getInventory().clear();
			p.getInventory().setArmorContents(null);
			cs.sendMessage(Strings.getString(Strings.COMMAND_CLEAR_SELF));
		} else if (args.length == 1) {
			User target = User.getUser(args[0]);
			if (target == null) {
				cs.sendMessage(Strings.getString(Strings.COMMAND_PLAYER_NOT_FOUND, new String[] { args[0] }));
			} else if (target.getPlayer() == null) {
				cs.sendMessage(
						Strings.getString(Strings.COMMAND_PLAYER_NOT_ONLINE, new String[] { target.formatUserName() }));
			} else {

				String sender = Strings.getString(Strings.GENERAL_CONSOLE);
				if (cs instanceof Player) {
					User u = User.getUser(cs.getName());
					if (u.getGroup().getPriority() < target.getGroup().getPriority()) {
						cs.sendMessage(Strings.getString(Strings.COMMAND_CLEAR_CAN_NOT,
								new String[] { target.formatUserName() }));
						return false;

					}
				}

				target.getPlayer().getInventory().clear();
				target.getPlayer().getInventory().setArmorContents(null);
				target.getPlayer()
						.sendMessage(Strings.getString(Strings.COMMAND_CLEAR_OTHER_MESSAGE, new String[] { sender }));
				cs.sendMessage(
						Strings.getString(Strings.COMMAND_CLEAR_OTHER, new String[] { target.formatUserName() }));
			}
		} else
			cs.sendMessage(Strings.getString(Strings.COMMAND_CLEAR_USAGE));

		return true;

	}
}

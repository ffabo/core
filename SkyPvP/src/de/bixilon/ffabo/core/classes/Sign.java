package de.bixilon.ffabo.core.classes;

import java.util.HashMap;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.inventory.ItemStack;

public class Sign {
	public static HashMap<Location, Sign> signs;
	public HashMap<User, Integer> used = new HashMap<User, Integer>();

	int id;
	Location loc;
	int delay;
	ItemStack item;
	int count;
	int levelcost;
	int permission;

	@SuppressWarnings("deprecation")
	public Sign(int id, Location loc, int delay, ItemStack item, int count, int levelcost, int permission) {
		this.id = id;
		this.loc = loc;
		this.delay = delay;
		this.item = item;
		this.count = count;
		this.levelcost = levelcost;
		this.permission = permission;
		// init
		Block b = loc.getBlock();
		if (b.getType() != Material.SIGN_POST && b.getType() != Material.WALL_SIGN) {
			Location block = loc;
			block.setZ(block.getZ() + 1);
			if (block.getBlock().getType().equals(Material.AIR))
				block.getBlock().setType(Material.STONE);
			b.setType(Material.WALL_SIGN);
		}

		org.bukkit.block.Sign s = (org.bukkit.block.Sign) b.getState();
		s.setLine(0, "§2Sky§4P§0v§4P");
		s.setLine(1, "§e" + count + "§3x" + item.getTypeId());
		if (levelcost == 0)
			s.setLine(2, "§1[Kostenlos]");
		else
			s.setLine(2, "§e" + levelcost + "§2x Level");
		if (permission != 0) {
			s.setLine(3, "§6[VIP]");
		}
		s.update();
	}

	public static void clearSigns() {
		signs = new HashMap<Location, Sign>();
	}

	public static void addSign(Sign s) {
		signs.put(s.getLocation(), s);
	}

	public Location getLocation() {
		return loc;
	}

	public int getPermission() {
		return permission;
	}

	public int getCost() {
		return levelcost;
	}

	public int getID() {
		return id;
	}

	public int getDelay() {
		return delay;
	}

	public ItemStack getItem() {
		return item;
	}

	public int getCount() {
		return count;
	}

	public static Sign getSign(Location l) {
		if (signs.containsKey(l))
			return signs.get(l);
		return null;
	}
}

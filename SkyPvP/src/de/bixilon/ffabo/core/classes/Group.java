package de.bixilon.ffabo.core.classes;

import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.scoreboard.Team;

public class Group {
	public static HashMap<Integer, Group> groups;

	int id;
	int prio; // 0 = highest = Console ; "MaxInt" = lowest. Players with lower priority can't
				// "ban"
				// player with the same or higher one.
				// console = prio 0 (max)
	String name;
	String prefix;
	String suffix;

	String tab_prefix;
	String tab_suffix;
	public Team t;

	List<Permission> permissions;

	public Group(int id, int prio, String name, String prefix, String suffix, String tab_prefix, String tab_suffix,
			List<Permission> permissions) {
		this.id = id;
		this.prio = prio;
		this.name = name;
		this.prefix = prefix;
		this.suffix = suffix;
		this.tab_prefix = tab_prefix;
		this.tab_suffix = tab_suffix;
		this.permissions = permissions; // mostly null
		t = Bukkit.getScoreboardManager().getMainScoreboard().registerNewTeam(name);
		t.setPrefix(afterSpace(tab_prefix));
		t.setSuffix(preSpace(tab_suffix));

	}

	public void setPermissions(List<Permission> permissions) {
		this.permissions = permissions;
	}

	public int getID() {
		return id;
	}

	public int getPriority() {
		return prio;
	}

	public String getName() {
		return name;
	}


	String afterSpace(String i) { // prefix
		if (i.length() == 0) {
			return i;
		} else if (i.length() == 2 && i.startsWith("§"))
			return i;
		return i + " ";
	}

	String preSpace(String i) { // suffix
		if (i.length() == 0) {
			return i;
		} else if (i.length() == 2 && i.startsWith("§"))
			return i;
		return " " + i;
	}

	public String getRawPrefix() {
		return prefix;
	}
	public String getRawSuffix() {
		return suffix;
	}

	public String getRawTabPrefix() {
		return tab_prefix;
	}

	public String getRawTabSuffix() {
		return tab_suffix;
	}


	public String getPrefix() {
		return afterSpace(prefix);
	}
	public String getSuffix() {
		return preSpace(suffix);
	}

	public String getTabPrefix() {
		return afterSpace(tab_prefix);
	}

	public String getTabSuffix() {
		return preSpace(tab_suffix);
	}

	public boolean hasPermission(int permission) {
		for (int i = 0; i < permissions.size(); i++) {
			if (permissions.get(i).getID() == permission || permissions.get(i).getID() == 0) {

				return true;
			}
		}
		return false;
	}

	public static void clearGroups() {

		for (Team a : Bukkit.getScoreboardManager().getMainScoreboard().getTeams()) {
			a.unregister();
		}
		groups = new HashMap<Integer, Group>();
	}

	public static void addGroup(Group g) {
		groups.put(g.getID(), g);
	}

	public void updateGroup() {
		groups.replace(getID(), this);
	}

}

package de.bixilon.ffabo.core.classes;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;

public class BlockTimeShift {

	Material m;
	byte data;
	Location l;

	@SuppressWarnings("deprecation")
	public BlockTimeShift(Block b) {
		m = b.getType();
		data = b.getData();
		l = b.getLocation();

	}

	public Material getType() {
		return m;
	}

	public byte getData() {
		return data;
	}

	public Location getLocation() {
		return l;
	}

}

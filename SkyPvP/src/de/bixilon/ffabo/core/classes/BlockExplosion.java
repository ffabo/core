package de.bixilon.ffabo.core.classes;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.block.Block;

public class BlockExplosion {

	public List<Block> affected_blocks;
	public List<BlockTimeShift> bts = new ArrayList<BlockTimeShift>();
	long time;

	public BlockExplosion(List<Block> a) {
		time = System.currentTimeMillis();
		affected_blocks = a;
		for (int i = 0; i < a.size(); i++) {
			bts.add(new BlockTimeShift(a.get(i)));
			if (a.get(i).getType() != Material.TNT)
				a.get(i).setType(Material.AIR); // no drops
		}

	}

	public long getTime() {
		return time;
	}

}

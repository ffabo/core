package de.bixilon.ffabo.core.classes;

import java.util.HashMap;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.TimerTask;
import java.util.UUID;

import de.bixilon.ffabo.core.main.Settings;
import de.bixilon.ffabo.core.main.Strings;

public class InFight extends TimerTask {
	public static HashMap<UUID, Integer> infight = new HashMap<UUID, Integer>(); // UUID, timestap

	public void run() {
		// every second

		HashMap<UUID, Integer> clone = infight;
		Object[] set = clone.keySet().toArray();
		for(int i = 0; i < clone.size(); i++) {
			UUID uuid = (UUID) set[i];
			Integer value = infight.get(uuid);
			if ((System.currentTimeMillis() / 1000) - value >= Long
					.parseLong((String) Settings.getSetting(4).getValue())) {
				Player p = Bukkit.getServer().getPlayer(uuid);
				if (p != null)
					p.sendMessage(Strings.getString(Strings.EVENT_INFIGHT_OUTFIGHT));
				infight.remove(uuid);
			}
		}

	}

}

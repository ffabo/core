package de.bixilon.ffabo.core.classes;

public class Setting {

	int id;
	String key;
	Object value;
	String comment;

	public Setting(int id, String key, Object value, String comment) {
		this.id = id;
		this.key = key;
		this.value = value;
		this.comment = comment;
	}

	public int getID() {
		return id;
	}

	public String getKey() {
		return key;
	}

	public Object getValue() {
		return value;
	}

	public String getComment() {
		return comment;
	}
}

package de.bixilon.ffabo.core.classes;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.scheduler.BukkitRunnable;

public class BlockExplosionTimer extends BukkitRunnable {

	public static List<BlockExplosion> explosionlist = new ArrayList<BlockExplosion>();
	static List<Material> blacklist = new ArrayList<Material>();

	public static void init() {
		blacklist.add(Material.TNT);

	}

	@SuppressWarnings("deprecation")
	public void run() {
		// every ¼ second

		List<BlockExplosion> clone = explosionlist;
		for (int i = 0; i < clone.size(); i++) {
			BlockExplosion be = clone.get(i);
			if (System.currentTimeMillis() - be.getTime() >= 5000) {
				for (int ii = 0; ii < be.affected_blocks.size(); ii++) {

					Block b = Bukkit.getWorld(be.affected_blocks.get(ii).getWorld().getName())
							.getBlockAt(be.affected_blocks.get(ii).getLocation());
					BlockTimeShift o = be.bts.get(ii);
					if (b.getType() == Material.AIR) {
						if (o.getType() == Material.AIR) {
							b.setType(Material.BEDROCK);
						} else {
							if (!blacklist.contains(o.getType())) {
								b.setType(o.getType());
								b.setData(o.getData());
							}
						}
						// b.setData(o.getData());
						List<Block> newList = be.affected_blocks;
						newList.remove(b);
						clone.get(i).affected_blocks = newList;
						List<BlockTimeShift> timelist = be.bts;
						timelist.remove(o);
						clone.get(i).bts = timelist;
						break;
					}
					if (ii == be.affected_blocks.size() - 1) {
						explosionlist.remove(i);
						break;
					}

				}
			}
		}

	}
}

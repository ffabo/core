package de.bixilon.ffabo.core.classes;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.scheduler.BukkitRunnable;

import de.bixilon.ffabo.core.main.Settings;

public class BuildTimer extends BukkitRunnable {
	public static HashMap<Location, Long> blocks_built = new HashMap<Location, Long>(); // Location, timestap

	public void run() {
		// every second

		HashMap<Location, Long> clone = blocks_built;
		Object[] set = clone.keySet().toArray();
		for (int i = 0; i < clone.size(); i++) {
			Location loc = (Location) set[i];
			Long time = clone.get(loc);
			if (System.currentTimeMillis() - time >= Integer
					.parseInt((String) Settings.getSetting(8).getValue()) * 1000) { 
				Bukkit.getWorld(loc.getWorld().getName()).getBlockAt(loc).setType(Material.AIR);
				//loc.getBlock().setType(Material.AIR);
				blocks_built.remove(loc);
				
			}

		}

	}

}

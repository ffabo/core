package de.bixilon.ffabo.core.classes;

import java.util.HashMap;

public class Permission {

	static HashMap<Integer, Permission> permissions;

	String description;
	int id;
	String name;

	public Permission(int id, String name, String dsc) {
		description = dsc;
		this.id = id;
		this.name = name;
	}

	public int getID() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public static void clearPermissions() {
		permissions = new HashMap<Integer, Permission>();
	}

	public static void addPermission(Permission p) {
		permissions.put(p.getID(), p);
	}

	public static Permission getPermission(int key) {
		if (permissions.containsKey(key))
			return permissions.get(key);
		return null;
	}

}

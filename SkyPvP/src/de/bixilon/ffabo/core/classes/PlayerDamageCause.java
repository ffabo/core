package de.bixilon.ffabo.core.classes;

import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

public class PlayerDamageCause {

	long time;
	User causer;
	DamageCause cause;
	int type;

	public PlayerDamageCause(User causer, DamageCause cause, int type) {
		time = System.currentTimeMillis();
		this.causer = causer;
		this.cause = cause;
		this.type = type;
	}

	public User getCauser() {
		return causer;
	}

	public long getTime() {
		return time;
	}

	public DamageCause getCause() {
		return cause;
	}
	public int getType() {
		//1 = show by arrow,....
		return type;
	}
}

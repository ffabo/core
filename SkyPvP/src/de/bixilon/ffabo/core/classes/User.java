package de.bixilon.ffabo.core.classes;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.entity.Player;

import de.bixilon.ffabo.core.events.ItemXPEvent;
import de.bixilon.ffabo.core.main.Settings;

public class User {

	static HashMap<UUID, User> users = new HashMap<UUID, User>();
	static HashMap<String, UUID> uuid_names = new HashMap<String, UUID>(); // username always in lowercase

	UUID uuid;
	String name;
	Player player;
	int rank;
	int firsttimeonline;
	int lasttimeonline;

	int kills;
	int deaths;

	String nickname; // null if not nicked
	boolean autonick;

	PlayerDamageCause pgc; // Latest damage cause by a PLAYER

	public User(String name, UUID uuid, Player player, int rank, int firsttimeonline, int lasttimeonline,
			boolean autonick, int kills, int deaths) {
		this.name = name;
		this.uuid = uuid;
		this.player = player;
		this.rank = rank;
		if (player != null)
			getGroup().t.addEntry(player.getName());
		this.firsttimeonline = firsttimeonline;
		this.lasttimeonline = lasttimeonline;
		this.autonick = autonick;
		this.kills = kills;
		this.deaths = deaths;

	}

	public Group getGroup() {
		return Group.groups.get(rank);
	}

	public boolean hasPermission(int permission) {
		return getGroup().hasPermission(permission);
	}

	public boolean isNicked() {
		if (nickname != null && nickname.length() >= 3 && nickname.length() <= 35)
			return true;
		return false;
	}

	public String getUserName() {
		return name;
	}

	public UUID getUUID() {
		return uuid;
	}

	public Player getPlayer() {
		return player;
	}

	public int getLastOnlineTime() {
		return lasttimeonline;
	}

	public static void clearUsers() {
		users = new HashMap<UUID, User>();
		uuid_names = new HashMap<String, UUID>();
	}

	public static void addUser(User u) {
		users.put(u.getUUID(), u);
		uuid_names.put(u.getUserName(), u.getUUID());
	}

	public static User getUser(String name) {
		return getUser(uuid_names.get(name));
	}

	public static User getUser(UUID uuid) {
		if (users.containsKey(uuid))
			return users.get(uuid);
		return null;
	}

	public void updateUser() {
		users.replace(getUUID(), this);
	}

	public void setUserName(String s) {
		if (!s.equals(getUserName())) {
			// username changed since last join
			uuid_names.remove(this.name);
			this.name = s;
			uuid_names.put(getUserName(), getUUID());
		}
	}

	public boolean getAutoNick() {
		return autonick;
	}

	public boolean isInFight() {
		if (hasPermission(11))
			return false;
		if (InFight.infight.containsKey(getUUID()))
			if ((System.currentTimeMillis() / 1000) - InFight.infight.get(getUUID()) < Long
					.parseLong((String) Settings.getSetting(4).getValue()))
				return true;
		return false;
	}

	public String formatUserName() {
		return getGroup().getPrefix() + getUserName() + getGroup().getSuffix();
	}

	public String formatTabUsername() {
		return getGroup().getTabPrefix() + getUserName() + getGroup().getTabSuffix();
	}

	public void setPlayer(Player p) {
		player = p;
	}

	public PlayerDamageCause getLastDamageCause() {
		return pgc;
	}

	public void setLastDamageCause(PlayerDamageCause pgc) {
		this.pgc = pgc;
	}

	public void updateLastOnlineTime() {
		lasttimeonline = (int) (System.currentTimeMillis() / 1000);
	}

	public void addKill() {
		if (getPlayer() != null) {
			getPlayer().setLevel(getPlayer().getLevel() + Integer.parseInt((String) Settings.getSetting(7).getValue()));
			ItemXPEvent.killEffect(getPlayer().getLocation());
			kills++;
		} else {
			// must edit nbt file
		}
	}

	public void addDeath() {
		deaths++;
	}

	public int getKills() {
		return kills;
	}

	public int getDeaths() {
		return deaths;
	}
}

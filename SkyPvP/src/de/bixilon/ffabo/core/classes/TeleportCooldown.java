package de.bixilon.ffabo.core.classes;

import java.util.HashMap;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.TimerTask;
import java.util.UUID;

import de.bixilon.ffabo.core.commands.Spawn;
import de.bixilon.ffabo.core.main.Settings;

public class TeleportCooldown extends TimerTask {
	public static HashMap<UUID, Integer> spawn_teleport = new HashMap<UUID, Integer>(); // UUID, timestap

	public void run() {
		// every second

		HashMap<UUID, Integer> clone = spawn_teleport;
		Object[] set = clone.keySet().toArray();
		for(int i = 0; i < clone.size(); i++) {
			UUID uuid = (UUID) set[i];
			Integer value = clone.get(uuid);
			if ((System.currentTimeMillis() / 1000) - value >= Long
					.parseLong((String) Settings.getSetting(3).getValue())) {
				Player p = Bukkit.getServer().getPlayer(uuid);
				if (p != null) {
					Spawn.teleportToSpawn(p);
				}
				spawn_teleport.remove(uuid);
			}
		}

	}

}

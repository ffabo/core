package de.bixilon.ffabo.core.mysql;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.TimerTask;

import de.bixilon.ffabo.core.main.Settings;

public class AntiDisconnect extends TimerTask {
	static Driver driver;

	public static void setDriver(Driver d) {
		driver = d;
	}

	public void run() {

		Statement s = driver.getStatement();
		try {
			s.execute("use " + Settings.mysql_db + ";");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}

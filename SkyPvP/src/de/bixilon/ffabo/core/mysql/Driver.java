package de.bixilon.ffabo.core.mysql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class Driver {
	Connection c;
	public boolean connected = true;

	public Driver(String host, int port, String db, String user, String pw) {
		c = create(host, port, db, user, pw);
		try {
			getStatement().execute("use " + db + ";");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	Connection create(String host, int port, String db, String user, String pw) {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			return DriverManager.getConnection("jdbc:mysql://" + host + ":" + port + "/" + db
					+ "?autoReconnect=true&user=" + user + "&password=" + pw);
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			connected = false;
			e.printStackTrace();
		}
		return null;
	}

	public Statement getStatement() {
		try {
			return c.createStatement();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}

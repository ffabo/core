package de.bixilon.ffabo.core.mysql;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.UUID;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import de.bixilon.ffabo.core.classes.Group;
import de.bixilon.ffabo.core.classes.Permission;
import de.bixilon.ffabo.core.classes.Serializer;
import de.bixilon.ffabo.core.classes.Setting;
import de.bixilon.ffabo.core.classes.Sign;
import de.bixilon.ffabo.core.classes.User;
import de.bixilon.ffabo.core.main.Main;
import de.bixilon.ffabo.core.main.Settings;
import de.bixilon.ffabo.core.main.Strings;

public class MySQL {
	Driver driver;

	public MySQL(Driver d) {
		driver = d;
		AntiDisconnect.setDriver(d);
		Timer timer = new Timer();
		timer.schedule(new AntiDisconnect(), 0, 60000);
	}

	public boolean isConnected() {
		return driver.connected;
	}

	public void fetch() {
		Statement s = driver.getStatement();
		try {
			// start with settings
			ResultSet settings_rs = s.executeQuery("SELECT * FROM `settings`");
			Settings.clearSettings();
			int count = 0;
			while (settings_rs.next()) {
				count++;
				Settings.addSetting(new Setting(settings_rs.getInt("id"), settings_rs.getString("key"),
						settings_rs.getObject("value"), settings_rs.getString("comment")));
			}
			Main.getInstance().getLogger().info("Found " + count + " settings.");
			// hopefully all settings where set. If not: Fatal error

			// permissions
			ResultSet permissions_rs = s.executeQuery("SELECT * FROM `permissions`");
			Permission.clearPermissions();
			count = 0;
			while (permissions_rs.next()) {
				count++;
				Permission.addPermission(new Permission(permissions_rs.getInt("id"), permissions_rs.getString("name"),
						permissions_rs.getString("description")));
			}
			Main.getInstance().getLogger().info("Found " + count + " permissions.");

			// groups

			ResultSet groups_rs = s.executeQuery("SELECT * FROM `groups`");
			Group.clearGroups();
			count = 0;
			while (groups_rs.next()) {
				count++;
				Group.addGroup(new Group(groups_rs.getInt("id"), groups_rs.getInt("priority"),
						groups_rs.getString("name"), groups_rs.getString("prefix"), groups_rs.getString("suffix"),
						groups_rs.getString("tab_prefix"), groups_rs.getString("tab_suffix"), null));
			}
			Main.getInstance().getLogger().info("Found " + count + " groups.");

			// group permissions
			for (int i = 1; i - 1 < Group.groups.size(); i++) {

				Group g = Group.groups.get(i);
				ResultSet group_permissions_rs = s
						.executeQuery("SELECT * FROM `group_permissions` WHERE `group` = " + g.getID() + ";");
				List<Permission> permissions = new ArrayList<Permission>();
				count = 0;
				while (group_permissions_rs.next()) {
					count++;
					Permission perm = Permission.getPermission(group_permissions_rs.getInt("permission"));
					if (perm != null) {
						permissions.add(perm);
					} else {
						// Ups! Unknown permission! Maybe an mistake or an editing error! Skipping for
						// now.
						Main.getInstance().getLogger().warning("§cUnknown Permission! Group: " + g.getID()
								+ "; Permission: " + group_permissions_rs.getInt("permission"));
					}
				}
				g.setPermissions(permissions);
				// saving group in hashmap...
				g.updateGroup();
			}
			Main.getInstance().getLogger().info("Found " + count + " group permissions.");

			// groups

			ResultSet sign_rs = s.executeQuery("SELECT * FROM `signs`");
			Sign.clearSigns();
			count = 0;
			while (sign_rs.next()) {
				count++;
				if (Settings.debug)
					Main.getInstance().getLogger()
							.info("Fetching sign: " + count + " (ID: " + sign_rs.getInt("id") + ")");
				ItemStack item = Serializer.itemStackArrayFromBase64(sign_rs.getString("item"))[0];

				Location loc = new Location(Bukkit.getWorld(sign_rs.getString("world")), sign_rs.getInt("x"),
						sign_rs.getInt("y"), sign_rs.getInt("z"));
				Sign sign = new Sign(sign_rs.getInt("id"), loc, sign_rs.getInt("delay"), item, sign_rs.getInt("count"),
						sign_rs.getInt("levelcost"), sign_rs.getInt("permission"));
				Sign.addSign(sign);
			}
			Main.getInstance().getLogger().info("Found " + count + " signs.");

			// users

			ResultSet users_rs = s.executeQuery("SELECT * FROM `users`");
			User.clearUsers();
			count = 0;
			while (users_rs.next()) {
				count++;
				User.addUser(new User(users_rs.getString("username"), UUID.fromString(users_rs.getString("uuid")),
						Bukkit.getPlayer(UUID.fromString(users_rs.getString("uuid"))), users_rs.getInt("group"),
						users_rs.getInt("firsttimeonline"), users_rs.getInt("lasttimeonline"),
						users_rs.getBoolean("autonick"), users_rs.getInt("kills"), users_rs.getInt("deaths")));
			}
			Main.getInstance().getLogger().info("Found " + count + " users.");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			Bukkit.getShutdownMessage();
			e.printStackTrace();
		}

	}

	public User newUser(Player p) { // returns user id
		Statement s = driver.getStatement();
		try {
			Setting default_group = Settings.getSetting(2);
			if (default_group == null) {
				p.kickPlayer(Strings.getString(Strings.LOGIN_CRITICAL_ERROR));
				return null;
			}
			int default_group_id = Integer.parseInt((String) default_group.getValue());
			int current_time = ((int) (System.currentTimeMillis() / 1000));

			s.execute(
					"INSERT INTO `users` (`uuid`, `username`, `group`, `firsttimeonline`, `lasttimeonline`, `onlinetime`, `autonick`, `kills`, `deaths`) VALUES ('"
							+ p.getUniqueId() + "', '" + p.getName() + "', '" + default_group_id + "', '" + current_time
							+ "', '" + current_time + "', '0', '0', '0', '0');");
			User u = new User(p.getName(), p.getUniqueId(), p, default_group_id, current_time, current_time, false, 0,
					0);
			return u;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;

	}

	public void updateUser(User u) {
		// user must exist already in database!
		Statement s = driver.getStatement();
		try {
			s.execute("UPDATE `users` SET `lasttimeonline` = '" + u.getLastOnlineTime() + "', `group` = "
					+ u.getGroup().getID() + ", `username` = '" + u.getUserName() + "', `autonick` = " + u.getAutoNick()
					+ ", `kills` = " + u.getKills() + ", `deaths` = " + u.getDeaths() + "   WHERE `uuid` = '"
					+ u.getUUID() + "';");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void setSpawnPoint(String loc) {

		Statement s = driver.getStatement();
		try {
			s.execute("UPDATE `settings` SET `value` = '" + loc + "' WHERE `id` = '5';");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}

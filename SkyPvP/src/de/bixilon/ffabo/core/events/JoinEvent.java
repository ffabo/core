package de.bixilon.ffabo.core.events;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import de.bixilon.ffabo.core.classes.User;
import de.bixilon.ffabo.core.commands.Build;
import de.bixilon.ffabo.core.commands.Spawn;
import de.bixilon.ffabo.core.main.Main;
import de.bixilon.ffabo.core.main.Strings;

public class JoinEvent implements Listener {

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerLogin(PlayerJoinEvent e) {
		// update mysql, apply rank, ...
		if (User.getUser(e.getPlayer().getUniqueId()) != null) {
			// user was already before on server
			User u = User.getUser(e.getPlayer().getUniqueId());
			u.setPlayer(e.getPlayer());
			u.setUserName(e.getPlayer().getName());
			u.getGroup().t.addEntry(u.getUserName());
			u.updateUser();
			u.updateLastOnlineTime();
			Main.mysql.updateUser(u);
			u.getPlayer().setPlayerListName(u.formatTabUsername());
		} else {
			// user was not before on server
			Player newPlayer = e.getPlayer();
			Main.getInstance().getLogger().info("New User(\"" + newPlayer.getName() + "\") joined the server!");

			User newUser = Main.mysql.newUser(newPlayer);
			if (newUser == null) {
				newPlayer.kickPlayer(Strings.getString(Strings.LOGIN_CRITICAL_ERROR));
				return;
			}
			User.addUser(newUser);
			newPlayer.teleport(Spawn.getSpawn());
		}
	}

	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent e) {
		Build.building.remove(e.getPlayer().getUniqueId());
	}

}

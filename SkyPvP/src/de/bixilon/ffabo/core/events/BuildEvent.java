package de.bixilon.ffabo.core.events;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.player.PlayerBucketEmptyEvent;
import org.bukkit.event.player.PlayerBucketFillEvent;

import de.bixilon.ffabo.core.classes.BuildTimer;
import de.bixilon.ffabo.core.classes.User;
import de.bixilon.ffabo.core.commands.Build;
import de.bixilon.ffabo.core.main.Main;
import de.bixilon.ffabo.core.main.Strings;

public class BuildEvent implements Listener {
	static List<Material> blacklist = new ArrayList<Material>();
	static List<Location> falling = new ArrayList<Location>();

	public static void init() {
		blacklist.add(Material.BARRIER);
		blacklist.add(Material.REDSTONE_WIRE);
		blacklist.add(Material.REDSTONE);
		blacklist.add(Material.REDSTONE_BLOCK);
		blacklist.add(Material.REDSTONE_TORCH_ON);
		blacklist.add(Material.REDSTONE_TORCH_OFF);
		blacklist.add(Material.REDSTONE_LAMP_OFF);
		blacklist.add(Material.REDSTONE_LAMP_ON);
		blacklist.add(Material.HOPPER);
		blacklist.add(Material.CHEST);
		blacklist.add(Material.SAPLING);
		blacklist.add(Material.SUGAR_CANE);
		blacklist.add(Material.SUGAR_CANE_BLOCK);
		blacklist.add(Material.MINECART);
		blacklist.add(Material.COMMAND);
		blacklist.add(Material.COMMAND_MINECART);
		blacklist.add(Material.EXPLOSIVE_MINECART);
		blacklist.add(Material.HOPPER_MINECART);
		blacklist.add(Material.POWERED_MINECART);
		blacklist.add(Material.STORAGE_MINECART);
		blacklist.add(Material.SPONGE);
		blacklist.add(Material.BED);
		blacklist.add(Material.LEVER);
		blacklist.add(Material.WOOD_PLATE);
		blacklist.add(Material.STONE_PLATE);
		blacklist.add(Material.IRON_PLATE);
		blacklist.add(Material.GOLD_PLATE);
		blacklist.add(Material.STONE_BUTTON);
		blacklist.add(Material.WOOD_BUTTON);
		blacklist.add(Material.DAYLIGHT_DETECTOR);
		blacklist.add(Material.DAYLIGHT_DETECTOR_INVERTED);
		blacklist.add(Material.REDSTONE_COMPARATOR);
		blacklist.add(Material.PISTON_BASE);
		blacklist.add(Material.PISTON_STICKY_BASE);
		blacklist.add(Material.TNT);
	}

	@EventHandler
	public void onBuild(BlockPlaceEvent e) {
		User u = User.getUser(e.getPlayer().getUniqueId());
		if (Build.building.contains(u.getUUID()))
			return;
		if (blacklist.contains(e.getBlock().getType())) {
			e.setCancelled(true);
			u.getPlayer().sendMessage(Strings.getString(Strings.EVENT_BUILD_BLACKLIST));
			return;
		}
		if (!e.getBlockReplacedState().getType().equals(Material.AIR)) {
			e.setCancelled(true);
			return;
		}

		if (SpawnEvent.isInSpawn(e.getBlock().getLocation())) {
			e.setCancelled(true);
			return;
		}
		if (e.getBlock().getType() == Material.TNT) {
			Location loc = new Location(e.getBlock().getLocation().getWorld(), e.getBlock().getLocation().getX() + 0.5,
					e.getBlock().getLocation().getY(), e.getBlock().getLocation().getZ() + 0.5);

			TNTPrimed tnt = loc.getWorld().spawn(loc, TNTPrimed.class);
			tnt.setFuseTicks(80);
			e.getBlock().setType(Material.AIR);
			return;
		}

		BuildTimer.blocks_built.put(e.getBlock().getLocation(), System.currentTimeMillis());

	}

	@EventHandler
	public void onBreak(BlockBreakEvent e) {
		User u = User.getUser(e.getPlayer().getUniqueId());
		if (Build.building.contains(u.getUUID())) {
			BuildTimer.blocks_built.remove(e.getBlock().getLocation());
			return;
		}
		if (!BuildTimer.blocks_built.containsKey(e.getBlock().getLocation())) {
			e.setCancelled(true);
			return;
		}
		BuildTimer.blocks_built.remove(e.getBlock().getLocation());
		e.getBlock().setType(Material.AIR);
	}

	@EventHandler
	public void onPlayerBucketEmpty(PlayerBucketEmptyEvent e) {
		User u = User.getUser(e.getPlayer().getUniqueId());
		if (Build.building.contains(u.getUUID()))
			return;
		e.setCancelled(true);
		u.getPlayer().sendMessage(Strings.getString(Strings.EVENT_BUILD_BLACKLIST));
	}

	@EventHandler
	public void onPlayerBucketFill(PlayerBucketFillEvent e) {
		User u = User.getUser(e.getPlayer().getUniqueId());
		if (Build.building.contains(u.getUUID()))
			return;
		e.setCancelled(true);
		u.getPlayer().sendMessage(Strings.getString(Strings.EVENT_BUILD_BLACKLIST));
	}

	public static void destroyAll() {

		HashMap<Location, Long> clone = BuildTimer.blocks_built;
		Object[] set = clone.keySet().toArray();
		int a = clone.size();
		for (int i = 0; i < a; i++) {
			Location loc = (Location) set[i];
			Main.getInstance().getLogger().info(
					"Breaking block at: X: " + loc.getBlockX() + ";Y: " + loc.getBlockY() + ";Z: " + loc.getBlockZ());
			BuildTimer.blocks_built.remove(loc);
			loc.getBlock().setType(Material.AIR);

		}
	}

	@EventHandler
	public void onFallingBlock(EntityChangeBlockEvent e) {
		if (e.getEntityType() == EntityType.FALLING_BLOCK) {

			if (BuildTimer.blocks_built.containsKey(e.getBlock().getLocation())) {
				falling.add(e.getBlock().getLocation());
				BuildTimer.blocks_built.remove(e.getBlock().getLocation());
				return;
			}

			if (!falling.contains(e.getBlock().getLocation())) {
				Location l = e.getBlock().getLocation();
				for (int i = l.getBlockY() + 1; i <= 256; i++) {
					l = l.add(0, 1, 0);
					if (falling.contains(l)) {
						BuildTimer.blocks_built.put(e.getBlock().getLocation(), System.currentTimeMillis());
						return;
					}
				}
			}
			e.setCancelled(true);
		}
	}

}

package de.bixilon.ffabo.core.events;

import org.bukkit.Bukkit;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import de.bixilon.ffabo.core.classes.InFight;
import de.bixilon.ffabo.core.classes.PlayerDamageCause;
import de.bixilon.ffabo.core.classes.User;
import de.bixilon.ffabo.core.commands.Spawn;
import de.bixilon.ffabo.core.main.Settings;
import de.bixilon.ffabo.core.main.Strings;

public class InFightEvent implements Listener {

	@EventHandler
	public void onDamage(EntityDamageByEntityEvent e) {
		if (e.isCancelled())
			return;
		if (e.getDamager() instanceof Player) {
			// player is attacker
			Player p = (Player) e.getDamager();
			if (SpawnEvent.isInSpawn(p.getLocation()))
				return;
			addFight(p);
			if (e.getEntity() instanceof Player) {
				User attacker = User.getUser(p.getUniqueId());
				User victim = User.getUser(((Player) e.getEntity()).getUniqueId());
				victim.setLastDamageCause(new PlayerDamageCause(attacker, e.getCause(), 0));
			}
		} else if (e.getDamager() instanceof Arrow) {
			// player is attacker
			if (((Arrow) e.getDamager()).getShooter() instanceof Player) {
				Player p = (Player) ((Arrow) e.getDamager()).getShooter();

				if (SpawnEvent.isInSpawn(p.getLocation()))
					return;
				addFight(p);
				if (e.getEntity() instanceof Player) {
					User attacker = User.getUser(p.getUniqueId());
					User victim = User.getUser(((Player) e.getEntity()).getUniqueId());
					victim.setLastDamageCause(new PlayerDamageCause(attacker, e.getCause(), 1));
				}
			}
		} else if (e.getEntity() instanceof Player) {
			User victim = User.getUser(((Player) e.getEntity()).getUniqueId());
			victim.setLastDamageCause(null);
		}

		if (e.getEntity() instanceof Player) {
			// player is victim
			Player p = (Player) e.getEntity();
			if (SpawnEvent.isInSpawn(p.getLocation()))
				return;
			addFight(p);

		}

	}

	@EventHandler
	public void onDamage(EntityDamageEvent e) {
		if (e.isCancelled())
			return;

		// player is victim
		if (e.getCause() == DamageCause.POISON || e.getCause() == DamageCause.FIRE
				|| e.getCause() == DamageCause.FIRE_TICK || e.getCause() == DamageCause.THORNS
				|| e.getCause() == DamageCause.WITHER) {
			if (e.getEntity() instanceof Player) {
				Player p = (Player) e.getEntity();
				if (SpawnEvent.isInSpawn(p.getLocation()))
					return;
				addFight(p);
			}
		}

	}

	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent e) {
		User u = User.getUser(e.getPlayer().getUniqueId());
		if (u.isInFight()) {
			// user is in fight....
			PlayerInventory pi = u.getPlayer().getInventory();
			pi.addItem(u.getPlayer().getInventory().getArmorContents());
			if (u.getLastDamageCause() != null)
				u.getLastDamageCause().getCauser().addKill();
			int newlvl = u.getPlayer().getLevel() - Integer.parseInt((String) Settings.getSetting(6).getValue());
			if (newlvl < 0)
				newlvl = 0;
			u.getPlayer().setLevel(newlvl);

			for (ItemStack i : pi) {
				if (i != null) {
					u.getPlayer().getWorld().dropItem(u.getPlayer().getLocation(), i);
					u.getPlayer().getInventory().remove(i);
				}
			}
			u.getPlayer().teleport(Spawn.getSpawn());
			u.addDeath();
			InFight.infight.remove(u.getUUID());
			if (u.getLastDamageCause() != null)
				Bukkit.broadcastMessage(Strings.getString(Strings.DEATH_QUIT_REFOUND,
						new String[] { u.formatUserName(), u.getLastDamageCause().getCauser().formatUserName() }));
			else
				Bukkit.broadcastMessage(Strings.getString(Strings.DEATH_NO_KILL, new String[] { u.formatUserName() }));

		}
	}

	void addFight(Player p) {
		if (User.getUser(p.getUniqueId()).hasPermission(11))
			return;
		if (InFight.infight.containsKey(p.getUniqueId())) {
			if ((System.currentTimeMillis() / 1000) - InFight.infight.get(p.getUniqueId()) > Long
					.parseLong((String) Settings.getSetting(4).getValue())) {
				p.sendMessage(Strings.getString(Strings.EVENT_INFIGHT_INFIGHT,
						new String[] { Settings.getSetting(4).getValue().toString() }));
			}
			InFight.infight.replace(p.getUniqueId(), (int) (System.currentTimeMillis() / 1000));

		} else {
			InFight.infight.put(p.getUniqueId(), (int) (System.currentTimeMillis() / 1000));
			p.sendMessage(Strings.getString(Strings.EVENT_INFIGHT_INFIGHT,
					new String[] { Settings.getSetting(4).getValue().toString() }));
		}
	}

}

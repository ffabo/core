package de.bixilon.ffabo.core.events;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.util.Vector;

public class BoostPlateEvent implements Listener {

	@EventHandler
	public void onPressurePlateStep(PlayerInteractEvent e) {
		Player p = e.getPlayer();
		if (!e.getAction().equals(Action.PHYSICAL) || e.getClickedBlock() == null
				|| e.getClickedBlock().getType() != Material.GOLD_PLATE)
			return;
		Block gold = e.getClickedBlock().getRelative(BlockFace.DOWN);
		if (!gold.getType().equals(Material.GOLD_BLOCK))
			return;

		p.playSound(p.getLocation(), Sound.WITHER_SHOOT, 3.0F, 2.0F);

		@SuppressWarnings("deprecation")
		int a = gold.getRelative(BlockFace.DOWN).getTypeId() * 5;
		if (p.isSprinting())
			a = a * 10;
		@SuppressWarnings("deprecation")
		Vector v = p.getLocation().getDirection().multiply(a).setY(gold.getRelative(BlockFace.DOWN).getTypeId());
		p.setVelocity(v);

	}

}

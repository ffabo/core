package de.bixilon.ffabo.core.events;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockSpreadEvent;

public class AntiEnvironmentChange implements Listener {
	@EventHandler
	public void onChange(BlockSpreadEvent e) {
		if (e.getNewState().getData().getItemType().equals(Material.VINE))
			e.setCancelled(true);
	}

}

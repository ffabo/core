package de.bixilon.ffabo.core.events;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import de.bixilon.ffabo.core.classes.User;

public class JoinLeaveMessage implements Listener {

	@EventHandler
	public void onPlayerLogin(PlayerJoinEvent e) {
		if (User.getUser(e.getPlayer().getUniqueId()) == null)
			e.setJoinMessage("§aWilllkommen auf dem Server, §e" + e.getPlayer().getName() + "§a!");
		else
			e.setJoinMessage("§6[§a+§6] " + User.getUser(e.getPlayer().getUniqueId()).formatUserName());
	}

	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent e) {
		e.setQuitMessage("§6[§c-§6] " + User.getUser(e.getPlayer().getUniqueId()).formatUserName());
	}

}

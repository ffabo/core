package de.bixilon.ffabo.core.events;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityExplodeEvent;

import de.bixilon.ffabo.core.classes.BlockExplosion;
import de.bixilon.ffabo.core.classes.BlockExplosionTimer;
import de.bixilon.ffabo.core.classes.BuildTimer;

public class ExplosionEvent implements Listener {

	@EventHandler
	public void onBlock(EntityExplodeEvent e) {
		List<Block> sorted = new ArrayList<Block>();
		int lowesty = 256;
		int highesty = 0;
		for (Block b : e.blockList()) {
			if (b.getY() < lowesty)
				lowesty = b.getY();
			if (b.getY() > highesty)
				highesty = b.getY();
		}
		for (int i = lowesty; i < highesty; i++) {
			for (Block b : e.blockList()) {
				if (b.getY() == i) {
					if (BuildTimer.blocks_built.containsKey(b.getLocation()))
						continue;
					sorted.add(b);
				}

			}
		}
		BlockExplosionTimer.explosionlist.add(new BlockExplosion(sorted));

	}
}

package de.bixilon.ffabo.core.events;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.inventory.ItemStack;

public class AntiFallDamage implements Listener {
	static ItemStack lapis;

	@EventHandler
	public void onFallDamage(EntityDamageEvent e) {
		if (e.getCause().equals(DamageCause.FALL))
			e.setCancelled(true);
	}

}

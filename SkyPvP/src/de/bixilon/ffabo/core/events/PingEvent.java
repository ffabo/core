package de.bixilon.ffabo.core.events;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.ServerListPingEvent;

import de.bixilon.ffabo.core.main.Settings;

public class PingEvent implements Listener {

	@EventHandler
	public void onServerPingEvent(ServerListPingEvent e) {
		e.setMotd((String) Settings.getSetting(1).getValue());
		e.setMaxPlayers(Integer.parseInt((String) Settings.getSetting(12).getValue()));
	}

}

package de.bixilon.ffabo.core.events;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import de.bixilon.ffabo.core.classes.Sign;
import de.bixilon.ffabo.core.classes.User;
import de.bixilon.ffabo.core.main.Strings;

public class SignEvent implements Listener {

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onSignClick(PlayerInteractEvent e) {
		if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
			Block b = e.getClickedBlock();
			if (b.getType() == Material.WALL_SIGN || b.getType() == Material.SIGN_POST) {
				org.bukkit.block.Sign sign = (org.bukkit.block.Sign) b.getState();
				Sign s = Sign.getSign(sign.getLocation());
				User u = User.getUser(e.getPlayer().getUniqueId());
				if(u.hasPermission(21)&&u.getPlayer().getItemInHand().getTypeId() == 280) {
					u.getPlayer().sendMessage("X: " + sign.getLocation().getBlockX() + "; Y: " + sign.getLocation().getBlockY() + "; Z: " + sign.getLocation().getBlockZ());
					return;
				}
				if (s == null)
					return;
				if (s.getPermission() != 0)
					if (!u.hasPermission(s.getPermission())) {
						u.getPlayer().sendMessage(Strings.getString(Strings.SIGN_NO_PERMISSION));
						return;
					}
				if (s.used.containsKey(u)) {
					int timeleft = s.getDelay() - ((int) ((System.currentTimeMillis() / 1000) - s.used.get(u))) - 1;
					if (timeleft >= 0) {
						u.getPlayer().sendMessage(Strings.getString(Strings.SIGN_PLEASE_WAIT,
								new String[] { String.valueOf(timeleft + 1) }));
						return;
					}

				}
				if (s.getCost() != 0) {
					if (u.getPlayer().getLevel() < s.getCost()) {
						u.getPlayer().sendMessage(Strings.getString(Strings.SIGN_NOT_ENOUGH_LEVEL,
								new String[] { String.valueOf(s.getCost()) }));
						return;
					}
					u.getPlayer().setLevel(u.getPlayer().getLevel() - s.getCost());
				}

				if (s.used.containsKey(u))
					s.used.replace(u, (int) (System.currentTimeMillis() / 1000));
				else
					s.used.put(u, (int) (System.currentTimeMillis() / 1000));
				if (u.getPlayer().isSneaking()) {
					ItemStack i = s.getItem();
					i.setAmount(s.getCount());
					u.getPlayer().getInventory().addItem(i);
					return;
				}
				// player not sneaking, opening gui
				Inventory gui = Bukkit.createInventory(null, 9, "");
				ItemStack i = s.getItem();
				if (s.getCount() <= 64) {
					i.setAmount(s.getCount());
					gui.setItem(4, i);
				} else if (s.getCount() <= 128) {
					i.setAmount(s.getCount() / 2);
					gui.setItem(0, i);
					gui.setItem(8, i);
				} else if (s.getCount() <= 192) {
					i.setAmount(s.getCount() / 3);
					gui.setItem(0, i);
					gui.setItem(4, i);
					gui.setItem(8, i);
				} else if (s.getCount() <= 256) {
					i.setAmount(s.getCount() / 4);
					gui.setItem(0, i);
					gui.setItem(1, i);
					gui.setItem(7, i);
					gui.setItem(8, i);
				} else if (s.getCount() <= 320) {
					i.setAmount(s.getCount() / 5);
					gui.setItem(0, i);
					gui.setItem(1, i);
					gui.setItem(4, i);
					gui.setItem(7, i);
					gui.setItem(8, i);
				} else
					return;

				u.getPlayer().openInventory(gui);

			}
		}
	}

}

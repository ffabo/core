package de.bixilon.ffabo.core.events;

import java.util.regex.Pattern;

import org.bukkit.Location;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Item;
import org.bukkit.entity.ItemFrame;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntitySpawnEvent;
import org.bukkit.event.player.PlayerJoinEvent;

import de.bixilon.ffabo.core.commands.Spawn;
import de.bixilon.ffabo.core.main.Settings;

public class SpawnEvent implements Listener {

	public static int x1;
	public static int x2;
	public static int y1;
	public static int y2;
	public static int z1;
	public static int z2;
	static String world;

	public static void init() {

		String[] raw1 = ((String) Settings.getSetting(9).getValue()).split(Pattern.quote(";"));
		String[] raw2 = ((String) Settings.getSetting(10).getValue()).split(Pattern.quote(";"));
		world = raw1[0];
		int xx1 = Integer.parseInt(raw1[1]);
		int xx2 = Integer.parseInt(raw2[1]);
		int yy1 = Integer.parseInt(raw1[2]);
		int yy2 = Integer.parseInt(raw2[2]);
		int zz1 = Integer.parseInt(raw1[3]);
		int zz2 = Integer.parseInt(raw2[3]);
		if (xx1 <= xx2) {
			x1 = xx1;
			x2 = xx2;
		} else {
			x1 = xx2;
			x2 = xx1;
		}
		if (yy1 <= yy2) {
			y1 = yy1;
			y2 = yy2;
		} else {
			y1 = yy2;
			y2 = yy1;
		}

		if (zz1 <= zz2) {
			z1 = zz1;
			z2 = zz2;
		} else {
			z1 = zz2;
			z2 = zz1;
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onDamage(EntityDamageEvent e) {
		if (e.getEntity() instanceof Player) {
			Player p = (Player) e.getEntity();
			if (isInSpawn(p.getLocation()))
				e.setCancelled(true);
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onDamage(EntityDamageByEntityEvent e) {
		if (e.getEntity() instanceof Player && e.getDamager() instanceof Player) {
			Player p = (Player) e.getEntity();
			Player d = (Player) e.getDamager();
			if (isInSpawn(p.getLocation()))
				e.setCancelled(true);
			if (isInSpawn(d.getLocation()))
				e.setCancelled(true);

		}
	}

	@EventHandler
	public void onMobSpawn(EntitySpawnEvent e) {
		if (!(e.getEntity() instanceof Player) && !(e.getEntity() instanceof ItemFrame)
				&& !(e.getEntity() instanceof ArmorStand) && !(e.getEntity() instanceof Item)) {
			if (isInSpawn(e.getLocation()))
				e.setCancelled(true);
		}

	}

	public static boolean isInSpawn(Location l) {
		if (!l.getWorld().getName().equals(world))
			return false;

		int lx = l.getBlockX();
		int ly = l.getBlockY();
		int lz = l.getBlockZ();

		boolean wasinx = false;
		boolean wasiny = false;
		boolean wasinz = false;

		if (lx > x1 && lx < x2)
			wasinx = true;
		if (lx == x1 || lx == x2)
			wasinx = true;

		if (ly > y1 && ly < y2)
			wasiny = true;
		if (ly == y1 || ly == y2)
			wasiny = true;

		if (lz > z1 && lz < z2)
			wasinz = true;
		if (lz == z1 || lz == z2)
			wasinz = true;

		if (wasinx && wasiny && wasinz)
			return true;

		return false;
	}

	@EventHandler
	public void onPlayerLogin(PlayerJoinEvent e) {
		e.getPlayer().teleport(Spawn.getSpawn());
	}
}

package de.bixilon.ffabo.core.events;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import de.bixilon.ffabo.core.classes.TeleportCooldown;
import de.bixilon.ffabo.core.main.Settings;
import de.bixilon.ffabo.core.main.Strings;

public class TeleportEvent implements Listener {

	@EventHandler
	public void onSpawn(PlayerMoveEvent e) {
		Player p = e.getPlayer();
		if (TeleportCooldown.spawn_teleport.containsKey(p.getUniqueId())
				&& (System.currentTimeMillis() / 1000) - TeleportCooldown.spawn_teleport.get(p.getUniqueId()) < Long
						.parseLong((String) Settings.getSetting(3).getValue())) {
			p.sendMessage(Strings.getString(Strings.COMMAND_SPAWN_TELEPORT_CANCELLED));
			TeleportCooldown.spawn_teleport.remove(p.getUniqueId());

		}
	}

}

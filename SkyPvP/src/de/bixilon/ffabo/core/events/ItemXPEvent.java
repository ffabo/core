package de.bixilon.ffabo.core.events;

import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.meta.FireworkMeta;

import de.bixilon.ffabo.core.classes.InFight;
import de.bixilon.ffabo.core.classes.PlayerDamageCause;
import de.bixilon.ffabo.core.classes.User;
import de.bixilon.ffabo.core.commands.Spawn;
import de.bixilon.ffabo.core.main.Settings;
import de.bixilon.ffabo.core.main.Strings;

public class ItemXPEvent implements Listener {

	@EventHandler
	public void onDeath(PlayerDeathEvent e) {

		// killed player
		User k = User.getUser(e.getEntity().getUniqueId());

		int newlvl = k.getPlayer().getLevel() - Integer.parseInt((String) Settings.getSetting(6).getValue());
		if (newlvl < 0)
			newlvl = 0;
		k.getPlayer().setLevel(newlvl);
		k.addDeath();
		e.setDroppedExp(0);
		e.setKeepLevel(true);
		if (!Bukkit.getWorld("world").isGameRule("keepInventory"))
			e.setKeepInventory(false);

		// killer

		PlayerDamageCause pdc = k.getLastDamageCause();
		k.setLastDamageCause(null);
		InFight.infight.remove(k.getUUID());

		if (pdc == null) {
			// no kill
			e.setDeathMessage(Strings.getString(Strings.DEATH_NO_KILL, new String[] { k.formatUserName() }));
			return;
		}
		if (k.equals(pdc.getCauser())) {
			// self killed
			e.setDeathMessage(Strings.getString(Strings.DEATH_NO_KILL, new String[] { k.formatUserName() }));
			return;
		}
		User a = pdc.getCauser();
		if (pdc.getCause() == DamageCause.FIRE || pdc.getCause() == DamageCause.FIRE_TICK) {
			if (System.currentTimeMillis() - pdc.getTime() <= ((k.getPlayer().getFireTicks() / 20) * 1000)) {
				e.setDeathMessage(Strings.getString(Strings.DEATH_KILLED_BURNED,
						new String[] { k.formatUserName(), a.formatUserName() }));
				a.addKill();
				return;
			}
			e.setDeathMessage(Strings.getString(Strings.DEATH_BURNED, new String[] { k.formatUserName() }));
		}
		if (pdc.getCause() == DamageCause.THORNS) {
			e.setDeathMessage(Strings.getString(Strings.DEATH_KILLED_ITEM,
					new String[] { k.formatUserName(), a.formatUserName() }));
			a.addKill();
			return;
		}
		if (pdc.getCause() == DamageCause.ENTITY_ATTACK) {
			switch (pdc.getType()) {
			case 1:
				// bow
				e.setDeathMessage(Strings.getString(Strings.DEATH_KILLED_BOW,
						new String[] { k.formatUserName(), a.formatUserName() }));
			default:
				e.setDeathMessage(Strings.getString(Strings.DEATH_KILLED_OTHER,
						new String[] { k.formatUserName(), a.formatUserName() }));

			}
			a.addKill();
			return;
		}
		e.setDeathMessage(
				Strings.getString(Strings.DEATH_KILLED_OTHER, new String[] { k.formatUserName(), a.formatUserName() }));
		a.addKill();
		return;

	}

	@EventHandler
	public void onRespawn(PlayerRespawnEvent e) {
		e.setRespawnLocation(Spawn.getSpawn());
	}

	public static void killEffect(Location l) {
		for (int i = 0; i < 5; i++) {

			// Spawn the Firework, get the FireworkMeta.
			Firework fw = (Firework) l.getWorld().spawnEntity(l, EntityType.FIREWORK);
			FireworkMeta fwm = fw.getFireworkMeta();

			// Our random generator
			Random r = new Random();

			// Get the type
			int rt = r.nextInt(4) + 1;
			Type type = Type.BALL;
			if (rt == 1)
				type = Type.BALL;
			if (rt == 2)
				type = Type.BALL_LARGE;
			if (rt == 3)
				type = Type.BURST;
			if (rt == 4)
				type = Type.CREEPER;
			if (rt == 5)
				type = Type.STAR;

			// Get our random colours
			int r1i = r.nextInt(17) + 1;
			int r2i = r.nextInt(17) + 1;
			Color c1 = getColor(r1i);
			Color c2 = getColor(r2i);

			// Create our effect with this
			FireworkEffect effect = FireworkEffect.builder().flicker(r.nextBoolean()).withColor(c1).withFade(c2)
					.with(type).trail(r.nextBoolean()).build();

			// Then apply the effect to the meta
			fwm.addEffect(effect);

			// Generate some random power and set it
			int rp = r.nextInt(2) + 1;
			fwm.setPower(rp);

			// Then apply this to our rocket
			fw.setFireworkMeta(fwm);
		}

	}

	public static Color getColor(int i) {
		Color c = null;
		if (i == 1) {
			c = Color.AQUA;
		}
		if (i == 2) {
			c = Color.BLACK;
		}
		if (i == 3) {
			c = Color.BLUE;
		}
		if (i == 4) {
			c = Color.FUCHSIA;
		}
		if (i == 5) {
			c = Color.GRAY;
		}
		if (i == 6) {
			c = Color.GREEN;
		}
		if (i == 7) {
			c = Color.LIME;
		}
		if (i == 8) {
			c = Color.MAROON;
		}
		if (i == 9) {
			c = Color.NAVY;
		}
		if (i == 10) {
			c = Color.OLIVE;
		}
		if (i == 11) {
			c = Color.ORANGE;
		}
		if (i == 12) {
			c = Color.PURPLE;
		}
		if (i == 13) {
			c = Color.RED;
		}
		if (i == 14) {
			c = Color.SILVER;
		}
		if (i == 15) {
			c = Color.TEAL;
		}
		if (i == 16) {
			c = Color.WHITE;
		}
		if (i == 17) {
			c = Color.YELLOW;
		}

		return c;
	}

}

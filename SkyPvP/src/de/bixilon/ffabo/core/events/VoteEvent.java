package de.bixilon.ffabo.core.events;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import com.vexsoftware.votifier.model.Vote;
import com.vexsoftware.votifier.model.VotifierEvent;

import de.bixilon.ffabo.core.classes.User;
import de.bixilon.ffabo.core.main.Main;
import de.bixilon.ffabo.core.main.Settings;
import de.bixilon.ffabo.core.main.Strings;

public class VoteEvent implements Listener {

	@EventHandler(priority = EventPriority.NORMAL)
	public void onVotifierEvent(VotifierEvent e) {
		Vote v = e.getVote();
		Main.getInstance().getLogger().info("New Vote: " + v.getUsername());
		if (v.getUsername().length() < 3) {
			Main.getInstance().getServer().broadcastMessage(Strings.getString(Strings.VOTE_UNKNOWN));
			return;
		}
		User u = User.getUser(v.getUsername());
		if (u == null) {
			Main.getInstance().getServer().broadcastMessage(Strings.getString(Strings.VOTE_NOT_FOUND));
			return;
		}
		if (u.getPlayer() == null) {
			// player not online
			Main.getInstance().getServer()
					.broadcastMessage(Strings.getString(Strings.VOTE_NOT_ONLINE, new String[] { u.formatUserName() }));
			return;
		}
		Main.getInstance().getServer()
				.broadcastMessage(Strings.getString(Strings.VOTE_BROADCAST, new String[] { u.formatUserName() }));
		u.getPlayer().setLevel(u.getPlayer().getLevel() + Integer.parseInt((String) Settings.getSetting(11).getValue()));

	}

}
package de.bixilon.ffabo.core.events;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import de.bixilon.ffabo.core.classes.User;

public class ChatEvent implements Listener {

	@EventHandler
	public void formatPlayer(AsyncPlayerChatEvent e) {
		e.setFormat(User.getUser(e.getPlayer().getUniqueId()).formatUserName() + "§a» §7" + e.getMessage());

	}

}

package de.bixilon.ffabo.core.events;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.ItemFrame;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBurnEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.EntityInteractEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.Dye;

import de.bixilon.ffabo.core.classes.User;
import de.bixilon.ffabo.core.commands.Build;

public class AntiBreakEvent implements Listener {
	static List<Material> blacklist = new ArrayList<Material>();
	static ItemStack bone_meal;

	public static void init() {
		Dye d = new Dye();
		d.setColor(DyeColor.WHITE);
		bone_meal = d.toItemStack();
	}

	@EventHandler
	public void onDamage(BlockBurnEvent e) {
		e.setCancelled(true);
	}

	@EventHandler
	public void onChange(PlayerInteractEntityEvent e) {
		User u = User.getUser(e.getPlayer().getUniqueId());
		if (Build.building.contains(u.getUUID()))
			return;
		// item frame
		if (e.getRightClicked() instanceof ItemFrame)
			e.setCancelled(true);
		// armor stand
		if (e.getRightClicked() instanceof ArmorStand)
			e.setCancelled(true);

	}

	@EventHandler
	public void onChange(EntityDamageEvent e) {
		if (e.getEntity() instanceof ItemFrame || e.getEntity() instanceof ArmorStand)
			e.setCancelled(true);
	}

	@EventHandler
	public void onChange(EntityInteractEvent e) {
		if (e.getEntity() instanceof ItemFrame || e.getEntity() instanceof ArmorStand)
			e.setCancelled(true);
	}

	@EventHandler
	public void noUproot(PlayerInteractEvent e) {
		if (e.getAction() == Action.PHYSICAL && e.getClickedBlock() != null
				&& e.getClickedBlock().getType() == Material.SOIL) {
			User u = User.getUser(e.getPlayer().getUniqueId());
			if (Build.building.contains(u.getUUID()))
				return;
			e.setCancelled(true);
		}
	}

	@EventHandler
	public void noBoneMeal(PlayerInteractEvent e) {
		if (e.getAction() == Action.RIGHT_CLICK_BLOCK && e.getItem() != null && e.getItem().isSimilar(bone_meal)) {
			User u = User.getUser(e.getPlayer().getUniqueId());
			if (Build.building.contains(u.getUUID()))
				return;
			e.setCancelled(true);
		}
	}

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onPlayerInteractWithAnvil(PlayerInteractEvent e) {
		if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
			if (e.getClickedBlock().getType() == Material.ANVIL) {
				int newD = 0;
				switch (e.getClickedBlock().getData()) {
				case 1:
					newD = 1;
				case 5:
					newD = 1;
				case 9:
					newD = 1;
				case 2:
					newD = 2;
				case 7:
					newD = 2;
				case 11:
					newD = 2;
				case 3:
					newD = 3;
				case 6:
					newD = 3;
				case 10:
					newD = 3;
				}

				e.getClickedBlock().setData((byte) newD);
				;
			}
		}
	}

	@EventHandler(ignoreCancelled = true)
	public void onEntityExplode(EntityExplodeEvent e) {
		Entity entity = e.getEntity();
		if (entity.getType() == EntityType.ENDER_CRYSTAL) {
			e.setCancelled(true);
		}
	}

	@EventHandler()
	public void onEntityDamageByEntity(EntityDamageByEntityEvent e) {
		Entity entity = e.getEntity();
		if (entity.getType() == EntityType.ENDER_CRYSTAL)
			e.setCancelled(true);
	}

}
